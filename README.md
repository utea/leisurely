# leisurely

#### 分布式脚手架
本例提供分布式脚手架，项目应用spring cloud,使用nacos作为注册中心，使用seata的at模式进行分布式事务管理

#### 技术栈

 **软件支持**  

[参考文档](https://gitee.com/utea/leisurely/wikis/%E6%82%A0%E9%97%B2?sort_id=3000943)

须有 JDK 11，mysql8,redis 
> 如果没有对应的组件的话请去除pom引入,如没有rabbitmq 就把pom去掉 配置引入去掉即可 sentinel，zipkin等类似

1. spring cloud 分布式架构
2. nacos 注册中心
3. seata 分布式服务治理
4. redis/redission 缓存机制 分布式锁支持
5. mybatis plus/shardingsphere 单数据库和读写分离支持 
6. rabbitMQ 消息队列
7. stomp websocket消息推送
8. sentinel限流，降级等等
9. zipkin链路追踪
10. task定时任务

> 应用架构
leisurely

    -- code-builder 代码生成器

    -- doc sql文件和其他说明文件

    -- platform 项目代码

        -- module 服务模块 

            -- usr 用户服务

            -- mq 消息队列服务

            -- stomp websocket消息推送服务

        -- common 公共模块

        -- gateway 网关

        -- pojo 各个模型统一放置模块


> 前端配套脚手架
[https://gitee.com/utea/utea-ui ](https://gitee.com/utea/utea-ui)

> 体系图
![](https://images.gitee.com/uploads/images/2020/1123/093150_9f6c6ec8_908360.png "屏幕截图.png")

> 部分功能截图
![新增菜单截图](https://images.gitee.com/uploads/images/2020/1124/131608_8048cd68_908360.png "屏幕截图.png")
![nacos](https://images.gitee.com/uploads/images/2020/1125/104129_0ce1240c_908360.png "屏幕截图.png")
![sentinel截图](https://images.gitee.com/uploads/images/2020/1124/131457_c94da370_908360.png "屏幕截图.png")
![Zipkin截图](https://images.gitee.com/uploads/images/2020/1125/104033_b876fde5_908360.png "屏幕截图.png")
#### 愿景
1、实现可学习性质的脚手架
2、应用上更多的相关技术框架
3、能坚持下来
