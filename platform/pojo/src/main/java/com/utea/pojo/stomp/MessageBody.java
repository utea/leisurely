package com.utea.pojo.stomp;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * stomp消息体
 */
@Data
@Accessors(chain = true)
public class MessageBody {
    /** 发送消息的用户 */
    private String from;
    /** 接收消息的用户 */
    private String to;
    /** 消息内容 */
    private String content;
    /** 消息类型 */
    private Integer type;
}
