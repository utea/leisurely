package com.utea.pojo.usr.login;

import lombok.Data;

@Data
public class LoginParam {
    private String account;
    private String password;
    private int key;
    private String code;
}
