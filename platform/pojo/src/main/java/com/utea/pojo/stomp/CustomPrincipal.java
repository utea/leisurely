package com.utea.pojo.stomp;


import lombok.Data;
import lombok.experimental.Accessors;

import java.security.Principal;

@Data
@Accessors(chain = true)
public class CustomPrincipal implements Principal {
    private String id;
    private String username;
    private int type;

    @Override
    public String getName() {
        return this.id;
    }
}
