package com.utea.pojo.usr.adminUsr;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("adminUser修改密码")
public class AdminUsrChangePwd {
    @ApiModelProperty(value = "原密码-md5加密",dataType = "String")
    @NotBlank
    private String oldPwd;
    @ApiModelProperty(value = "新密码-md5加密",dataType = "String")
    @NotBlank
    private String newPwd;
}
