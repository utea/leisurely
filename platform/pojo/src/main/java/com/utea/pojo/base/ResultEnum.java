package com.utea.pojo.base;

/**
 * 数据操作结果定义
 */
public enum ResultEnum {
    SUCCESS(200, "nice"),
    NO_AUTH(401,"你能不能先登录一下"),
    TOKEN_ERROR(401,"无效token"),
    CHECK_ERROR(402,"自行校验错误或异常的状态"),
    NO_PERMISSION(403,"你没得权限"),
    NOT_FOUND(404, "未找到该资源!"),
    NOT_SIGN(405, "无效的签名"),
    INTERNAL_SERVER_ERROR(500, "服务器跑路了"),
    ;

    /** 错误码 */
    private final Integer code;

    /** 错误信息 */
    private final String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
