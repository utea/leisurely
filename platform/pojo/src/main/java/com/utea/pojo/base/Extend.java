package com.utea.pojo.base;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * pojo拓展属性
 */
@Data
@Accessors(chain = true)
public class Extend {
    /**
     * 每页条数
     */
    private int size=10;
    /**
     * 第几页
     */
    private int current=1;
    /**
     * 降序字段数组
     *
     * @return order by desc 的字段数组
     */
    private String[] desc;
    /**
     * 升序字段数组
     *
     * @return order by asc 的字段数组
     */
    private String[] asc;
    /**
     * 模糊查询字段
     */
    private String like;
}
