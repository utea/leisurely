package com.utea.pojo.task;

import com.utea.pojo.base.BasePojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 定时任务详情表
 * </p>
 *
 * @author utea
 * @since 2020-11-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="TaskInfo对象", description="定时任务详情表")
public class TaskInfo extends BasePojo {


    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "时间corn")
    private String cron;

    @ApiModelProperty(value = "任务说明")
    private String remark;

    @ApiModelProperty(value = "任务调度api接口")
    private String api;

    @ApiModelProperty(value = "是否使用0否1是")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

    @ApiModelProperty(value = "api接口类型:1get,2post")
    private Integer apiMethod;

    @ApiModelProperty(value = "接口参数，json字符串")
    private String param;

}
