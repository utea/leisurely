package com.utea.pojo.usr.login;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class LoginResult {
    private String name;
    private String userId;
    private String token;
    private String avatar;
    private List<String> access;
}
