package com.utea.pojo.usr.adminUsr;

import com.utea.pojo.base.BasePojo;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Data
@Accessors(chain = true)
public class AdminUsr extends BasePojo {

    /**
     * 主键
     */
    private String id;

    /**
     * 名称
     */
    private String username;

    /**
     * 性别0女1男
     */
    private Integer sex;

    /**
     * 年纪
     */
    private Integer age;

    /**
     * 11位手机号
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 盐
     */
    private String salt;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 状态1正常，2禁用
     */
    private Integer status;
}
