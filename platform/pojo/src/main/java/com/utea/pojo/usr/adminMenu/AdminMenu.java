package com.utea.pojo.usr.adminMenu;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 管理后台菜单
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AdminMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 类型1目录2菜单3按钮4外链
     */
    private Integer type;

    /**
     * 唯一标志
     */
    private String identity;

    /**
     * 外链url
     */
    private String url;

    /**
     * 页面路由
     */
    private String route;

    /**
     * 时间
     */
    private String createTime;

    /**
     * 父级id
     */
    private String parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态1正常2开发3停用
     */
    private Integer status;

    /**
     * 图标
     */
    private String icon;
    private String component;


}
