package com.utea.pojo.base;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BasePojo {
    /**
     * 分页
     */
    @TableField(exist = false)
    private Extend extend;
}
