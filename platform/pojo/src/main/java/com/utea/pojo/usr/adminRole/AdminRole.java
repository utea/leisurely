package com.utea.pojo.usr.adminRole;

import java.io.Serializable;

import com.utea.pojo.base.BasePojo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 管理平台角色表
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AdminRole extends BasePojo {

    /**
     * 主键
     */
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 说明
     */
    private String remark;

    /**
     * 创建时间
     */
    private String createTime;


}
