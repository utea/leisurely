package com.utea.pojo.base;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 返回结果类
 */
@Data
@Accessors(chain = true)
public class JsonResult<T> {
    //是否成功
    private Boolean success;
    //状态码
    private Integer code;
    //提示信息
    private String msg;
    //数据
    private T data;
    public JsonResult() {

    }
    public JsonResult(T data) {
        this.success = true;
        this.code = ResultEnum.SUCCESS.getCode();
        this.msg = ResultEnum.SUCCESS.getMsg();
        this.data = data;
    }
    //自定义返回结果的构造方法
    public JsonResult(Boolean success,Integer code, String msg,T data) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    //自定义返回结果的构造方法
    public JsonResult(Boolean success,Integer code, String msg) {
        this.success = success;
        this.code = code;
        this.msg = msg;
    }
    //自定义异常返回的结果
    public static JsonResult defineError(DefinitionException de){
        JsonResult<String> result = new JsonResult<>();
        result.setSuccess(false);
        result.setCode(de.getErrorCode());
        result.setMsg(de.getErrorMsg());
        result.setData(null);
        return result;
    }
    //其他异常处理方法返回的结果
    public static JsonResult<String> otherError(ResultEnum errorEnum){
        JsonResult<String> result = new JsonResult<>();
        result.setMsg(errorEnum.getMsg());
        result.setCode(errorEnum.getCode());
        result.setSuccess(false);
        result.setData(null);
        return result;
    }
    public static <T> JsonResult<T> success(T data){
        return new JsonResult<>(data);
    }
    public JsonResult<T> error(String msg){
        this.code = ResultEnum.CHECK_ERROR.getCode();//自定义返回
        this.success = false;
        this.msg = msg ;
        return this;
    }
}
