package com.utea.pojo.usr.adminUsr;

import com.utea.pojo.base.BasePojo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author utea
 * @since 2020-11-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class AdminUsrRole extends BasePojo {


    /**
     * 主键
     */
    private String id;

    /**
     * adminUsrId
     */
    private String adminUsrId;

    /**
     * adminRoleId
     */
    private String adminRoleId;

    /**
     * 创建时间
     */
    private String createTime;


}
