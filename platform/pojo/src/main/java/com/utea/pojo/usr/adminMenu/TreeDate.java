package com.utea.pojo.usr.adminMenu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(description = "菜单树节点")
public class TreeDate extends AdminMenu{
    @ApiModelProperty(value = "子类菜单节点列表",dataType = "list")
    private List<TreeDate> children;
    @ApiModelProperty(value = "是否展开",dataType = "boolean")
    private boolean expand;
    @ApiModelProperty(value = "复选框是否勾选",dataType = "boolean")
    private boolean checked;
}
