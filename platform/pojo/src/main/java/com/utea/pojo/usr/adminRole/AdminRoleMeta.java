package com.utea.pojo.usr.adminRole;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 管理平台角色表
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Data
@Accessors(chain = true)
public class AdminRoleMeta{
    private String title;
    private String[] access;
    private String icon;
    private String href;
    private boolean hideInMenu;
}
