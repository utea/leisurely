package com.utea.common.global.error;

import com.utea.pojo.base.DefinitionException;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.base.ResultEnum;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义异常
     *
     */
    @ExceptionHandler(value = DefinitionException.class)
    @ResponseBody
    public JsonResult bizExceptionHandler(DefinitionException e) {
        e.printStackTrace();
        return JsonResult.defineError(e);
    }

    /**
     * 处理其他异常
     *
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public JsonResult exceptionHandler( Exception e) {
        e.printStackTrace();
        return JsonResult.otherError(ResultEnum.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {NoClassDefFoundError.class,ClassNotFoundException.class})
    @ResponseBody
    public JsonResult exceptionNoHandler( Exception e) {
        e.printStackTrace();
        return JsonResult.otherError(ResultEnum.NOT_FOUND);
    }
}