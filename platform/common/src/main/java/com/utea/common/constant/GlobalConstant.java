package com.utea.common.constant;

/**
 * 全局常量配置
 */
public interface GlobalConstant {
    interface ACTIVE{
        int DOWN = 0;//失效
        int UP = 1;//存活 有效
    }
}
