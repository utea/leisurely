package com.utea.common.global.token;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.utea.common.global.annotation.authFrom.FromEnum;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Data
public class TokenClaim {

    /**
     * 主键
     */
    private String id;

    /**
     * 名称
     */
    private String username;

    /**
     * 性别0女1男
     */
    private Integer sex;

    /**
     * 年纪
     */
    private Integer age;

    /**
     * 11位手机号
     */
    private String phone;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 用户登陆方式 web，app之类的
     * 可以用于区分用户操作的路口，后续可以做入口限制 或者判断等
     */
    private String from;

    public static JSONObject instance(Object usr) {
        return instance(usr, FromEnum.WEB.getFrom());
    }
    public static JSONObject instance(Object usr,String from) {
        return JSONUtil.parseObj(usr).set("from",from);
    }
    public static TokenClaim instanceBean(JSONObject jo){
        return JSONUtil.toBean(jo,TokenClaim.class);
    }
}
