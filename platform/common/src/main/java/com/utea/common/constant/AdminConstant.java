package com.utea.common.constant;

/**
 * 管理平台常量维护
 */
public interface AdminConstant {
    interface ADMIN_MENU_TYPE{
        int DIRECTORY = 1;
        int MENU = 2;
        int BUTTON = 3;
        int HREF = 4;
    }
}
