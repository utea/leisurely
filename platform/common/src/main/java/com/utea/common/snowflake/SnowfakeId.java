package com.utea.common.snowflake;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 使用hutool雪花id
 * 为了避免重复要使用单例模式
 * 可以交给spring IOC实现单例
 * snowflake.datacenterId在集群部署的时候 要记得配置区分 要不然会出现重复的情况
 */
@Component
public class SnowfakeId {
    private final Snowflake instance;

    /**
     * 不做公共处理 这样避免其他人直接new 导致重复
     * @param application name
     * @param datacenterId 1
     */
    private SnowfakeId(@Value("${spring.application.name}")String application,@Value("${snowflake.datacenterId:1}") int datacenterId){
        instance = IdUtil.createSnowflake(application.hashCode() % 32,datacenterId);
    }

    public String id(){
        return String.valueOf(instance.nextId());
    }
}
