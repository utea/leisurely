package com.utea.common.global.annotation.authFrom;

import java.lang.annotation.*;

/**
 * 对接口类型请求来源限定
 * 比如限制只能给 app调用 或者app来源的请求不可用
 * 此注解必须登陆 未登录全部都不允许通过 提示无权访问
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthFrom {
    FromEnum[] enable() default FromEnum.DEFAULT;
    FromEnum[] disable() default FromEnum.DEFAULT;
}
