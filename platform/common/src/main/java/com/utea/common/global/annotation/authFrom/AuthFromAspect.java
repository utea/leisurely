package com.utea.common.global.annotation.authFrom;

import com.utea.pojo.base.JsonResult;
import com.utea.pojo.base.ResultEnum;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import static com.utea.common.global.token.TokenMsg.operator;

@Aspect
@Component
public class AuthFromAspect {
    @Pointcut("@annotation(com.utea.common.global.annotation.authFrom.AuthFrom)")
    public void authFromPointcut() {}

    @Around("authFromPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        if(Optional.ofNullable(operator()).isEmpty()){//未登录判断
            return JsonResult.otherError(ResultEnum.NO_AUTH);
        }
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        Method method = signature.getMethod();
        AuthFrom authFrom = method.getAnnotation(AuthFrom.class);
        FromEnum[] enable = authFrom.enable();
        FromEnum[] disable = authFrom.disable();
        if(Optional.of(enable).filter(e->e.length==1&&"default".equals(e[0].getFrom())).isEmpty()){
            //不是默认进行处理 判断
            if(Arrays.stream(enable).noneMatch(f->f.getFrom().equals(operator().getFrom()))){
                return JsonResult.otherError(ResultEnum.NOT_FOUND);
            }
        }
        if(Optional.of(disable).filter(e->e.length==1&&"default".equals(e[0].getFrom())).isEmpty()){
            //不是默认进行处理 判断
            if(Arrays.stream(disable).anyMatch(f->f.getFrom().equals(operator().getFrom()))){
                return JsonResult.otherError(ResultEnum.NOT_FOUND);
            }
        }
        return joinPoint.proceed();
    }
}
