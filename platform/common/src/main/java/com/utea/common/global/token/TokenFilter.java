package com.utea.common.global.token;

import cn.hutool.json.JSONUtil;
import com.utea.common.utils.JwtUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

public class TokenFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //获取token
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        Optional.ofNullable(request.getHeader("token"))
                .filter(JwtUtils::verifyToken)
                .map(token-> JSONUtil.toBean(JSONUtil.parseObj(JwtUtils.parseToken(token)),TokenClaim.class))
                .ifPresent(TokenMsg::set);
        filterChain.doFilter(servletRequest,servletResponse);
        TokenMsg.clear();
    }

    @Override
    public void destroy() {

    }
}
