package com.utea.common.global.token;

/**
 * 当前登陆用户token对应的存储信息
 */
public class TokenMsg {

    private static final ThreadLocal<TokenClaim> local = new ThreadLocal<>();

    static void set(TokenClaim tokenClaim){
        local.set(tokenClaim);
    }
    public  static TokenClaim operator(){
        return local.get();
    }
    static void clear(){
        local.remove();
    }
}
