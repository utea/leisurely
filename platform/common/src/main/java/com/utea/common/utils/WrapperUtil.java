package com.utea.common.utils;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * Wrapper 构建工具
 */
public class WrapperUtil {

    private WrapperUtil(){}

    public static <T> QueryWrapper<T> queryWrapper(T obj){
        if(obj==null){
            return null;
        }
        QueryWrapper<T> qw = new QueryWrapper<>();
        String like = String.valueOf(ReflectUtil.getFieldValue(ReflectUtil.getFieldValue(obj,"extend"),"like"));
        Arrays.stream(ReflectUtil.getFields(obj.getClass())).filter(f->!f.getName().equals("extend"))
                .forEach(f->{
                    Optional.ofNullable(ReflectUtil.getFieldValue(obj,f)).ifPresent(v->{
                        Optional.ofNullable(like).filter(l->l.contains(f.getName()))
                            .ifPresentOrElse(l->qw.like(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, f.getName()),v)
                            ,()->qw.eq(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, f.getName()),v));
                    });
                });
        return qw;
    }
}
