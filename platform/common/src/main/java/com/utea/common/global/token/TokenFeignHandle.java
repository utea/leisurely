package com.utea.common.global.token;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Enumeration;
import java.util.Optional;

public class TokenFeignHandle implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        Optional.ofNullable(RequestContextHolder.getRequestAttributes())
                .map(attributes ->(ServletRequestAttributes) attributes )
                .map(ServletRequestAttributes::getRequest)
                .ifPresent(request->{
                    Enumeration<String> headerNames = request.getHeaderNames();
                    if (headerNames != null) {
                        while (headerNames.hasMoreElements()) {
                            String name = headerNames.nextElement();
                            String values = request.getHeader(name);
                            template.header(name, values);
                        }
                    }
                });
    }
}
