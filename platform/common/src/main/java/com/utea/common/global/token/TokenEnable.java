package com.utea.common.global.token;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 添加token 转换 当前登陆用户信息使用
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({TokenFilter.class,TokenFeignHandle.class})
public @interface TokenEnable {

}
