package com.utea.common.global.annotation.authFrom;

public enum FromEnum {
    DEFAULT("default","默认不限制"),
    APP("app","app端请求"),
    WEB("web","web端请求");

    private final String from;
    private final String remark;

    FromEnum(String from,String remark){
        this.from = from;
        this.remark = remark;
    }
    public String getFrom(){
        return this.from;
    }
}
