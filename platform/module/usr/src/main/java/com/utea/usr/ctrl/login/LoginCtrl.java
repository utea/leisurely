package com.utea.usr.ctrl.login;

import cn.hutool.json.JSONUtil;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.usr.login.LoginParam;
import com.utea.pojo.usr.login.LoginResult;
import com.utea.usr.service.adminUsr.AdminUsrService;
import com.utea.usr.service.webclient.WebClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class LoginCtrl {
    @Autowired
    private AdminUsrService adminUsrService;
    @Autowired
    private WebClientUtil webClientUtil;

    @PostMapping("/login")
    public JsonResult<LoginResult> login(@RequestBody LoginParam loginParam){
        return adminUsrService.login(loginParam);
    }
    @GetMapping("/checkToken/{token}")
    public JsonResult<String> checkToken(@PathVariable("token") String token){
        return adminUsrService.checkToken(token);
    }

    @GetMapping("/test")
    public void test(){
        System.out.println("get");
    }
    @PostMapping("/test1")
    public void test1(@RequestBody Object o){
        System.out.println("post"+ JSONUtil.toJsonStr(o));
    }



    public static void main(String[] args) {
        String s = "LEETCODEISHIRING";
        int numRows = 3;
        List<List<Integer>> l1 = new ArrayList<>();
        String[] ss = s.split("");
        List<Integer> sl = new ArrayList<>();
        for(int i=0;i<ss.length;i++){
            if(sl.size()==numRows){
                l1.add(sl);
                sl = new ArrayList<>();
            }

        }
    }
}
