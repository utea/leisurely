package com.utea.usr.service.adminMenu;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.utea.common.constant.AdminConstant;
import com.utea.common.snowflake.SnowfakeId;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.usr.adminMenu.AdminMenu;
import com.utea.pojo.usr.adminMenu.TreeDate;
import com.utea.pojo.usr.adminRole.AdminRoleMeta;
import com.utea.pojo.usr.adminRole.AdminRoute;
import com.utea.usr.dao.adminMenu.AdminMenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.utea.common.global.token.TokenMsg.operator;

/**
 * <p>
 * 管理后台菜单 服务类
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Service
public class AdminMenuService extends ServiceImpl<AdminMenuDao, AdminMenu> {

    @Autowired
    private SnowfakeId snowfakeId;

    /**
     * 新增菜单
     * @param adminMenu
     * @return
     */
    @Transactional
    public AdminMenu saveMenu(AdminMenu adminMenu){
        if(adminMenu.getSort()==null){
            adminMenu.setSort(1);
        }
        if(adminMenu.getParentId()==null){
            adminMenu.setParentId("0");
        }
        adminMenu.setId(snowfakeId.id()).setStatus(1).setCreateTime(DateUtil.now());
        getBaseMapper().insert(adminMenu);
        return adminMenu;
    }

    /**
     * 获取菜单路由
     * @return
     */
    public JsonResult<List<AdminRoute>> getRouter() {
        List<AdminMenu> menuList = Optional.of(operator().getId()).filter(id->!id.equals("1"))
                .map(id->getBaseMapper().getRouter(id))
                .orElse(getBaseMapper().selectList(null));
        if(menuList.size()>0){
            //先取出顶级菜单
            return new JsonResult<>(children(menuList,"0"));
        }else
            return new JsonResult<>(Collections.emptyList());
    }

    private List<AdminRoute> children(List<AdminMenu> menus,String parentId){
        return menus.stream().filter(m->m.getParentId().equals(parentId)).map(m-> {
            AdminRoute ar = init(m);
            if(m.getType() == AdminConstant.ADMIN_MENU_TYPE.DIRECTORY || m.getType() == AdminConstant.ADMIN_MENU_TYPE.MENU)
                ar.setChildren(children(menus, m.getId()));
            return ar;
        }).collect(Collectors.toList());
    }

    private AdminRoute init(AdminMenu m){
        AdminRoute ar = new AdminRoute();
        if(m.getType() == AdminConstant.ADMIN_MENU_TYPE.HREF){
            ar.setMeta(new AdminRoleMeta().setHideInMenu(false).setTitle(m.getTitle()).setHref(m.getUrl()).setIcon(m.getIcon()));
        }else{
            ar.setPath(m.getRoute().startsWith("/")?m.getRoute():"/"+m.getRoute())
                    .setComponent(m.getComponent())
                    .setName(m.getRoute().startsWith("/")?m.getRoute().substring(1):m.getRoute())
                    .setMeta(new AdminRoleMeta().setHideInMenu(false).setTitle(m.getTitle()).setIcon(m.getIcon()));
            if(m.getType() == AdminConstant.ADMIN_MENU_TYPE.BUTTON){
                ar.getMeta().setHideInMenu(true);
            }
        }
        return ar;
    }

    /**
     * 查询菜单管理页面的 总菜单树
     * @return
     */
    public JsonResult<List<TreeDate>> findTreeDate(){
        List<AdminMenu> menuList = getBaseMapper().selectList(null);
        //定义一个顶级节点  id = 0
        return new JsonResult<>(childrenTreeDate(menuList,"0"));
    }

    private List<TreeDate> childrenTreeDate(List<AdminMenu> menus,String parentId){
        return menus.stream().filter(m->parentId.equals(m.getParentId())).map(m-> {
            TreeDate td = new TreeDate().setExpand(true);
            BeanUtil.copyProperties(m,td);
            if(m.getType() == AdminConstant.ADMIN_MENU_TYPE.DIRECTORY || m.getType() == AdminConstant.ADMIN_MENU_TYPE.MENU)
                td.setChildren(childrenTreeDate(menus, m.getId()));
            return td;
        }).collect(Collectors.toList());
    }

    private List<TreeDate> coverTreeDate(List<TreeDate> menus,String parentId){
        return menus.stream().filter(m->parentId.equals(m.getParentId())).peek(td-> {
            td.setExpand(true);
            if(td.getType() == AdminConstant.ADMIN_MENU_TYPE.DIRECTORY || td.getType() == AdminConstant.ADMIN_MENU_TYPE.MENU)
                td.setChildren(coverTreeDate(menus, td.getId()));
        }).collect(Collectors.toList());
    }

    /**
     * 角色管理分配权限-查询权限树
     * @param id
     * @return
     */
    public JsonResult<List<TreeDate>> findRoleMenu(String id) {
        List<TreeDate> list = getBaseMapper().findRoleMenu(id);
        return JsonResult.success(coverTreeDate(list,"0"));
    }
}
