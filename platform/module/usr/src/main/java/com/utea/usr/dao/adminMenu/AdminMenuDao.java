package com.utea.usr.dao.adminMenu;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.utea.pojo.usr.adminMenu.AdminMenu;
import com.utea.pojo.usr.adminMenu.TreeDate;
import org.apache.ibatis.annotations.Param;

import java.awt.*;
import java.util.List;

/**
 * <p>
 * 管理后台菜单 Mapper 接口
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
public interface AdminMenuDao extends BaseMapper<AdminMenu> {
    List<TreeDate> findRoleMenu(@Param("roleId") String roleId);

    List<AdminMenu> getRouter(@Param("usrId") String usrId);
}
