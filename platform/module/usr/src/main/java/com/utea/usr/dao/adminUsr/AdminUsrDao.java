package com.utea.usr.dao.adminUsr;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.utea.pojo.usr.adminUsr.AdminUsr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
public interface AdminUsrDao extends BaseMapper<AdminUsr> {

}
