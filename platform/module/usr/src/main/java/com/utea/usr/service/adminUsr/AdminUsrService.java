package com.utea.usr.service.adminUsr;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.utea.common.snowflake.SnowfakeId;
import com.utea.common.utils.JwtUtils;
import com.utea.common.utils.RedisUtil;
import com.utea.common.utils.WrapperUtil;
import com.utea.pojo.base.Extend;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.base.ResultEnum;
import com.utea.pojo.usr.adminUsr.AdminUsr;
import com.utea.common.global.token.TokenClaim;
import com.utea.pojo.usr.adminUsr.AdminUsrChangePwd;
import com.utea.pojo.usr.adminUsr.AdminUsrRole;
import com.utea.pojo.usr.login.LoginParam;
import com.utea.pojo.usr.login.LoginResult;
import com.utea.usr.dao.adminUsr.AdminUsrDao;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.utea.common.global.token.TokenMsg.operator;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Service
public class AdminUsrService extends ServiceImpl<AdminUsrDao, AdminUsr> {

    @Autowired
    private SnowfakeId snowfakeId;
    @Autowired
    private AdminUsrRoleService adminUsrRoleService;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * 用户分页查询
     * @param adminUsr
     * @return
     */
    public JsonResult<IPage<AdminUsr>> findPage(AdminUsr adminUsr) {
        Extend extend = Optional.ofNullable(adminUsr).map(AdminUsr::getExtend).orElse(new Extend());
        Page<AdminUsr> page = new Page<AdminUsr>(extend.getCurrent(),extend.getSize())
                .setAsc(extend.getAsc()).setDesc(extend.getDesc());
        IPage<AdminUsr> result = getBaseMapper().selectPage(page, WrapperUtil.queryWrapper(adminUsr));
        Optional.ofNullable(result.getRecords()).filter(CollUtil::isNotEmpty).ifPresent(r->{
            result.setRecords(r.stream().peek(l->l.setPassword(null).setSalt(null)).collect(Collectors.toList()));
        });
        return JsonResult.success(result);
    }

    /**
     * 添加用户
     * @param adminUsr
     * @return
     */
    public JsonResult<Boolean> addAdminUsr(AdminUsr adminUsr){
        adminUsr.setId(snowfakeId.id()).setCreateTime(DateUtil.now()).setStatus(1)
                .setSalt(RandomUtil.randomString(8))
                .setPassword(SecureUtil.md5(SecureUtil.md5("123456")+adminUsr.getSalt()));
        return JsonResult.success(getBaseMapper().insert(adminUsr)==1);
    }

    /**
     * web登陆
     * @param loginParam
     * @return
     */
    public JsonResult<LoginResult> login(LoginParam loginParam) {
        //用户登陆
        AdminUsr usr = getBaseMapper().selectOne(WrapperUtil.queryWrapper(new AdminUsr().setUsername(loginParam.getAccount())));
        if(usr==null){
            return new JsonResult<LoginResult>().error("用户或密码错误");
        }
        if(!usr.getPassword().equals(SecureUtil.md5(loginParam.getPassword()+usr.getSalt()))){
            return new JsonResult<LoginResult>().error("用户或密码错误");
        }
        //生成token
        String token = JwtUtils.createToken(TokenClaim.instance(usr));
        LoginResult lr = new LoginResult().setName(usr.getUsername()).setUserId(usr.getId())
                .setAvatar("https://file.iviewui.com/dist/a0e88e83800f138b94d2414621bd9704.png")
                .setToken(token);
        return JsonResult.success(lr);
    }

    /**
     * 获取当前登陆用户明细
     * @return
     */
    public JsonResult<AdminUsr> getInfo() {
        return Optional.ofNullable(getBaseMapper().selectById(operator().getId()))
                .map(u->
                    JsonResult.success(u.setPassword(null).setSalt(null))
                ).orElse(new JsonResult<AdminUsr>().error("用户不存在"));
    }

    /**
     * 用户授权角色
     * @param usrId
     * @param roleIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public JsonResult<Boolean> authRole(String usrId, List<String> roleIds) {
        //先删除原先角色
        adminUsrRoleService.remove(WrapperUtil.queryWrapper(new AdminUsrRole().setAdminUsrId(usrId)));
        Optional.ofNullable(roleIds).filter(CollUtil::isNotEmpty).ifPresent(l->{
            String nd = DateUtil.now();
            adminUsrRoleService.saveBatch(l.stream().map(r->
                new AdminUsrRole().setId(snowfakeId.id()).setAdminUsrId(usrId).setAdminRoleId(r).setCreateTime(nd)
            ).collect(Collectors.toList()));
        });
        return JsonResult.success(true);
    }

    /**
     * 校验token
     * token 规则
     * 1、参考 jwt过期规则 如jwt是60分钟
     *  过期时间 = 有效期+缓冲时间
     *  有效 = 过期/2
     * 2、token失效场景
     *  a.token 验证无效
     *  b.token 验证有效 但超过有效期
     * 3、有效期截至前5分钟 进行token刷新替换
     * @param token
     * @return
     */
    public JsonResult<String> checkToken(String token) {
        return Optional.of(token).filter(JwtUtils::verifyToken)
                .map(t->JSONUtil.parseObj(JwtUtils.parseToken(t)))
                .map(jo->{
                    System.out.println(jo);
                    long nt = DateUtil.currentSeconds();
                    if(nt>(jo.getLong("iat")+(JwtUtils.TOKEN_EXPIRE_MILLIS/2000))){
                        return JsonResult.otherError(ResultEnum.TOKEN_ERROR);
                    } else if(nt>(jo.getLong("iat")+(JwtUtils.TOKEN_EXPIRE_MILLIS/2000)-5*60)){
                        //刷新token
                        jo.remove("exp");
                        jo.remove("iat");
                        return JsonResult.success(JwtUtils.createToken(jo));
                    }
                    return JsonResult.success("");
                })
                .orElse(JsonResult.otherError(ResultEnum.TOKEN_ERROR));
    }
    public void test() {
        // 锁测试
        Flux.range(0,10).flatMap(i-> Mono.fromRunnable(()->reentrantLock(i)).subscribeOn(Schedulers.elastic())).subscribe();
    }
    /**
     * 可重入锁
     * @param str
     */
    private void reentrantLock(Integer str){
        RLock lock = redissonClient.getLock("anyLock");
        try{
            // 1. 最常见的使用方法
            //lock.lock();

            // 2. 支持过期解锁功能,10秒钟以后自动解锁, 无需调用unlock方法手动解锁
            //lock.lock(10, TimeUnit.SECONDS);

            // 3. 尝试加锁，最多等待3秒，上锁以后10秒自动解锁
            boolean res = lock.tryLock(100, 20, TimeUnit.SECONDS);
            if(res){    //成功
                // do your business
                redisUtil.set(String.valueOf(str),DateUtil.date());
                System.out.println(str+"-in-"+DateUtil.date());
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println(str+"-out-"+DateUtil.date());
            lock.unlock();
        }
    }

    /**
     * 用户修改密码
     * 登陆的用户才能
     * @param adminUsrChangePwd
     * @return
     */
    public JsonResult<String> changePwd(AdminUsrChangePwd adminUsrChangePwd) {
        return Optional.of(this.getById(operator().getId()))
                .filter(u->u.getPassword().equals(SecureUtil.md5(adminUsrChangePwd.getOldPwd()+u.getSalt())))
                .map(u-> {
                    if(this.updateById(new AdminUsr().setId(u.getId()).setPassword(SecureUtil.md5(adminUsrChangePwd.getNewPwd()+u.getSalt())))){
                        return JsonResult.success("修改成功");
                    }else return new JsonResult<String>().error("设置错误");
                })
                .orElse(new JsonResult<String>().error("用户原密码错误"));
    }
}
