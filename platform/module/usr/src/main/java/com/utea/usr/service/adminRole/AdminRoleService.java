package com.utea.usr.service.adminRole;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.utea.common.snowflake.SnowfakeId;
import com.utea.common.utils.WrapperUtil;
import com.utea.pojo.base.Extend;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.usr.adminRole.AdminRole;
import com.utea.pojo.usr.adminRole.AdminRoleMenu;
import com.utea.pojo.usr.adminUsr.AdminUsr;
import com.utea.usr.dao.adminRole.AdminRoleDao;
import com.utea.usr.dao.adminRole.AdminRoleMenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 * 管理平台角色表 服务类
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Service
public class AdminRoleService extends ServiceImpl<AdminRoleDao, AdminRole> {
    @Autowired
    private SnowfakeId snowfakeId;
    @Autowired
    private AdminRoleMenuService adminRoleMenuService;

    /**
     * 角色分页查询
     * @param adminRole
     * @return
     */
    public JsonResult<IPage<AdminRole>> findPage(AdminRole adminRole) {
        Extend extend = Optional.ofNullable(adminRole).map(AdminRole::getExtend).orElse(new Extend());
        Page<AdminRole> page = new Page<AdminRole>(extend.getCurrent(),extend.getSize())
                .setAsc(extend.getAsc()).setDesc(extend.getDesc());
        IPage<AdminRole> result = getBaseMapper().selectPage(page, WrapperUtil.queryWrapper(adminRole));
        return JsonResult.success(result);
    }

    /**
     * 新增角色
     * @param adminRole
     * @return
     */
    public JsonResult<Boolean> saveAdminRole(AdminRole adminRole){
        adminRole.setCreateTime(DateUtil.now()).setId(snowfakeId.id());
        return JsonResult.success(getBaseMapper().insert(adminRole)==1);
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public JsonResult<Boolean> delAdminRole(String id) {
        //先删除记录 后续 补充完整
        getBaseMapper().deleteById(id);
        return JsonResult.success(true);
    }

    /**
     * 角色添加权限
     * @param roleId
     * @param menuIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public JsonResult<Boolean> addAuth(String roleId, List<String> menuIds){
        //先删除原先的记录
        LambdaQueryWrapper<AdminRoleMenu> w = new LambdaQueryWrapper<>();
        w.eq(AdminRoleMenu::getRoleId,roleId);
        adminRoleMenuService.remove(w);
        //进行批量添加记录
        Optional.ofNullable(menuIds).filter(CollUtil::isNotEmpty)
                .ifPresent(l->{
                    String nd = DateUtil.now();
                    adminRoleMenuService.saveBatch(l.stream().map(m->
                            new AdminRoleMenu().setId(snowfakeId.id()).setRoleId(roleId).setAdminMenuId(m).setCreateTime(nd)
                    ).collect(Collectors.toList()));
                });
        return JsonResult.success(true);
    }

    /**
     * 用户通过名称模糊查询角色列表最多5条
     * @param name
     * @return
     */
    public JsonResult<List<AdminRole>> adminUsrFindRoleList(String name) {
        Page<AdminRole> page = new Page<>(1,5,false);
        LambdaQueryWrapper<AdminRole> query = new LambdaQueryWrapper<>();
        query.like(AdminRole::getName,name);
        return JsonResult.success(getBaseMapper().selectPage(page,query).getRecords());
    }

    /**
     * 查询用户已有角色列表
     * @param usrId
     * @return
     */
    public JsonResult<List<AdminRole>> findUsrRoleList(String usrId) {
        List<AdminRole> list = getBaseMapper().findUsrRoleList(usrId);
        return JsonResult.success(list);
    }
}
