package com.utea.usr.dao.adminUsr;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.utea.pojo.usr.adminUsr.AdminUsrRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author utea
 * @since 2020-11-12
 */
public interface AdminUsrRoleDao extends BaseMapper<AdminUsrRole> {

}
