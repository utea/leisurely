package com.utea.usr.ctrl.adminMenu;


import com.utea.pojo.base.JsonResult;
import com.utea.pojo.usr.adminMenu.AdminMenu;
import com.utea.pojo.usr.adminMenu.TreeDate;
import com.utea.pojo.usr.adminRole.AdminRoute;
import com.utea.usr.service.adminMenu.AdminMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 管理后台菜单 前端控制器
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/adminMenu")
@Api(tags = "菜单管理模块")
public class AdminMenuCtrl {
    @Autowired
    private AdminMenuService adminMenuService;
    @GetMapping("/getRouter")
    public JsonResult<List<AdminRoute>> getRouter(){
        return adminMenuService.getRouter();
    }
    @GetMapping("/findTreeDate")
    public JsonResult<List<TreeDate>> findTreeDate(){
        return adminMenuService.findTreeDate();
    }

    @ApiOperation("用户查询角色菜单")
    @GetMapping("/findRoleMenu/{id}")
    public JsonResult<List<TreeDate>> findRoleMenu(@PathVariable("id") String id){
        return adminMenuService.findRoleMenu(id);
    }

    @PostMapping("/addMenu")
    public JsonResult<AdminMenu> addMenu(@RequestBody AdminMenu adminMenu){
        return new JsonResult<>(adminMenuService.saveMenu(adminMenu));
    }
    @PostMapping("/updMenu")
    public JsonResult<Object> updMenu(@RequestBody AdminMenu adminMenu){
        adminMenuService.updateById(adminMenu);
        return new JsonResult<>();
    }
    @GetMapping("/delMenu/{id}")
    public JsonResult<Object> delMenu(@PathVariable("id") String id){
        adminMenuService.removeById(id);
        return new JsonResult<>();
    }
}

