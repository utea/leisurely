package com.utea.usr.service.adminUsr;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.utea.pojo.usr.adminUsr.AdminUsrRole;
import com.utea.usr.dao.adminUsr.AdminUsrRoleDao;
import org.springframework.stereotype.Service;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author utea
 * @since 2020-11-12
 */
@Service
public class AdminUsrRoleService extends ServiceImpl<AdminUsrRoleDao, AdminUsrRole> {

}
