package com.utea.usr;

import com.utea.common.global.token.TokenEnable;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication(scanBasePackages = {"com.utea.usr", "com.utea.common"})
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan("com.utea.usr.dao")
@TokenEnable
public class UsrApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsrApplication.class, args);
    }

}
