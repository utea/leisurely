package com.utea.usr.service.adminRole;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.utea.pojo.usr.adminRole.AdminRoleMenu;
import com.utea.usr.dao.adminRole.AdminRoleMenuDao;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理平台角色-菜单表 服务类
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@Service
public class AdminRoleMenuService extends ServiceImpl<AdminRoleMenuDao, AdminRoleMenu> {

}
