package com.utea.usr.dao.adminRole;

import com.utea.pojo.usr.adminRole.AdminRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.awt.*;
import java.util.List;

/**
 * <p>
 * 管理平台角色表 Mapper 接口
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
public interface AdminRoleDao extends BaseMapper<AdminRole> {

    List<AdminRole> findUsrRoleList(@Param("usrId") String usrId);
}
