package com.utea.usr.ctrl.adminUsr;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.utea.common.global.annotation.authFrom.AuthFrom;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.usr.adminUsr.AdminUsr;
import com.utea.pojo.usr.adminUsr.AdminUsrChangePwd;
import com.utea.usr.service.adminUsr.AdminUsrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/adminUsr")
public class AdminUsrCtrl {

    @Autowired
    private AdminUsrService adminUsrService;


    @GetMapping("/getInfo")
    @AuthFrom()
    public JsonResult<AdminUsr> getInfo(){
        return adminUsrService.getInfo();
    }
    @GetMapping("/logout")
    public JsonResult<String> logout(){
        return new JsonResult<>();
    }
    @PostMapping("/findPage")
    public JsonResult<IPage<AdminUsr>> findPage(@RequestBody AdminUsr adminUsr){
        return adminUsrService.findPage(adminUsr);
    }
    @PostMapping("/addAdminUsr")
    public JsonResult<Boolean> addAdminUsr(@RequestBody AdminUsr adminUsr){
        return adminUsrService.addAdminUsr(adminUsr);
    }
    @PostMapping("/updAdminUsr")
    public JsonResult<Boolean> updAdminUsr(@RequestBody AdminUsr adminUsr){
        adminUsr.setPassword(null).setSalt(null);
        return JsonResult.success(adminUsrService.updateById(adminUsr));
    }
    @GetMapping("/delAdminUsr/{id}")
    public JsonResult<Object> delAdminUsr(@PathVariable("id") String id){
        if("1".equals(id)){
            return new JsonResult<>().error("admin不能删除");//管理员不允许删除
        }
        return JsonResult.success(adminUsrService.removeById(id));
    }
    @PostMapping("/authRole/{usrId}")
    public JsonResult<Boolean> authRole(@PathVariable("usrId")String usrId, @RequestBody List<String> roleIds){
        return adminUsrService.authRole(usrId,roleIds);
    }
    @PostMapping("/changePwd")
    public JsonResult<String> changePwd(@RequestBody @Valid AdminUsrChangePwd adminUsrChangePwd){
        return adminUsrService.changePwd(adminUsrChangePwd);
    }
}

