package com.utea.usr.ctrl.adminRole;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 管理平台角色-菜单表 前端控制器
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/adminRoleMenu")
public class AdminRoleMenuCtrl {

}

