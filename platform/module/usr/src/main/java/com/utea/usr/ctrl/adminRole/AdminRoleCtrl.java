package com.utea.usr.ctrl.adminRole;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.usr.adminRole.AdminRole;
import com.utea.usr.service.adminRole.AdminRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 管理平台角色表 前端控制器
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/adminRole")
public class AdminRoleCtrl {
    @Autowired
    private AdminRoleService adminRoleService;

    @PostMapping("/findPage")
    public JsonResult<IPage<AdminRole>> findPage(@RequestBody AdminRole adminRole){
        return adminRoleService.findPage(adminRole);
    }
    @GetMapping("/adminUsrFindRoleList/{name}")
    public JsonResult<List<AdminRole>> adminUsrFindRoleList(@PathVariable("name")String name){
        return adminRoleService.adminUsrFindRoleList(name);
    }
    @GetMapping("/findUsrRoleList/{usrId}")
    public JsonResult<List<AdminRole>> findUsrRoleList(@PathVariable("usrId")String usrId){
        return adminRoleService.findUsrRoleList(usrId);
    }

    @PostMapping("/addAdminRole")
    public JsonResult<Boolean> addAdminRole(@RequestBody AdminRole adminRole){
        return adminRoleService.saveAdminRole(adminRole);
    }
    @PostMapping("/updAdminRole")
    public JsonResult<Boolean> updAdminRole(@RequestBody AdminRole adminRole){
        return JsonResult.success(adminRoleService.updateById(adminRole));
    }
    @GetMapping("/delAdminRole/{id}")
    public JsonResult<Boolean> delAdminRole(@PathVariable("id") String id){
        return adminRoleService.delAdminRole(id);
    }
    @PostMapping("/addAuth/{roleId}")
    public JsonResult<Boolean> addAuth(@PathVariable("roleId")String roleId, @RequestBody List<String> menuIds){
        return adminRoleService.addAuth(roleId,menuIds);
    }
}

