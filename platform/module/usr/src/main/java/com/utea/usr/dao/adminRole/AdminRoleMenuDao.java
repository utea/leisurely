package com.utea.usr.dao.adminRole;

import com.utea.pojo.usr.adminRole.AdminRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理平台角色-菜单表 Mapper 接口
 * </p>
 *
 * @author utea
 * @since 2020-10-21
 */
public interface AdminRoleMenuDao extends BaseMapper<AdminRoleMenu> {

}
