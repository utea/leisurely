package com.utea.nosql.ctrl;

import com.utea.nosql.service.MongoDbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mongo")
public class MongoDbCtrl {

    @Autowired
    private MongoDbService mongoDbService;

    @GetMapping("/test")
    public void test(){
        mongoDbService.test();
    }
}
