package com.utea.nosql.service;

import com.utea.nosql.dao.EsRepository;
import com.utea.nosql.pojo.EsTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class EsService {
    @Autowired
    private EsRepository esRepository;

    public void test() {
        esRepository.save(new EsTest().setId("1").setTitle("tesetsd").setContent("sdf23idfksdfif地方"));
    }

    public List<EsTest> find(){
        List<EsTest> r = new ArrayList<>();
        Optional.of(esRepository.findAll()).ifPresent(i-> {
            i.forEach(r::add);
        });
        return r;
    }
}
