package com.utea.nosql.conf;

import com.utea.nosql.pojo.EsTest;
import com.utea.nosql.service.EsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/es")
@RestController
public class EsCtrl {
    @Autowired
    private EsService esService;

    @GetMapping("/test")
    public List<EsTest> test(){
        esService.test();
        return esService.find();
    }
}
