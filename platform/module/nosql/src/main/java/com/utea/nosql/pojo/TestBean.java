package com.utea.nosql.pojo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Accessors(chain = true)
public class TestBean {
    private String id;
    private String name;
    @Transient
    private String noSet;
}
