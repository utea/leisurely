package com.utea.nosql.dao;

import com.utea.nosql.pojo.EsTest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

@Component
public interface EsRepository extends ElasticsearchRepository<EsTest,String> {

}
