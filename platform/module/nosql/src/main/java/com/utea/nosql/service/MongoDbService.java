package com.utea.nosql.service;

import com.utea.nosql.pojo.TestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MongoDbService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Transactional
    public void test(){
        mongoTemplate.insert(new TestBean().setId("123123").setName("test").setNoSet("noset"));
        throw new RuntimeException();
    }
}
