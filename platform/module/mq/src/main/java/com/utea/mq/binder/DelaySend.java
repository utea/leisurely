package com.utea.mq.binder;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

//@EnableBinding 指信道channel和exchange绑定在一起
//@EnableBinding(Source.class) 就是将 Source(源) 放到 Channel 的意思
@EnableBinding(Delay.class)
public class DelaySend {
    @Resource
    @Output(Delay.OUTPUT_DELAY)//消息发送通道指定自定义路口
    private MessageChannel output; // 消息发送管道

    public void send() {
        String serial = UUID.randomUUID().toString();
        //延迟发送
        //output.send(MessageBuilder.withPayload(serial).setHeader("x-delay",5*1000).build());
        for(int i=0;i<10;i++){
            output.send(MessageBuilder.withPayload(i % 2==0?"delay":"channel")
                    .setHeader("x-delay",5*1000)
                    .setHeader("ssss",i % 2==0?"delay":"channel").build());
            System.out.println("发送消息: "+(i % 2==0?"delay":"channel"));
        }
    }

    @StreamListener(Delay.INPUT_DELAY)
    public void input(Message<String> message, @Header(AmqpHeaders.CHANNEL) Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) Long deliveryTag) {
        System.out.println(channel);
        System.out.println(deliveryTag);
        System.out.println("delay延迟消费者，接收："+message.getPayload()+new Date());
        try {
            //requeue=false 表示丢弃，
            // multiple表示批量处理，批量处理这个队列之前的消息统一处理结果 如都车工/失败，都丢弃
            channel.basicAck(deliveryTag,false);
        } catch (IOException e) {
            try {
                //channel.basicNack(deliveryTag, false, true);//表示deliveryTag的消息处理失败且将该消息重新放回队列
                channel.basicReject(deliveryTag, true);//表示deliveryTag的消息处理失败且将该消息重新放回队列,requeue=false 表示丢弃。
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            e.printStackTrace();
        }
    }
}
