package com.utea.mq.binder;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 延迟队列测试
 */
public interface Delay {
    String INPUT_DELAY = "inputDelay";
    String OUTPUT_DELAY = "outputDelay";

    @Input(INPUT_DELAY)
    SubscribableChannel inputDelay();

    @Output(OUTPUT_DELAY)
    MessageChannel outputDelay();
}
