package com.utea.mq.binder;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;

import java.io.IOException;
import java.util.Date;

@EnableBinding(RouteChannel.class)
public class RouteChannelBinder {

    @StreamListener(RouteChannel.INPUT_CHANNEL)
    public void inputChannel(Message<String> message) {
        System.out.println("channel延迟消费者，接收："+message.getPayload()+new Date());
    }
}
