package com.utea.mq.binder;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

//@EnableBinding 指信道channel和exchange绑定在一起
//@EnableBinding(Source.class) 就是将 Source(源) 放到 Channel 的意思
@EnableBinding(Deadline.class)
public class DeadlineBinder {
    @Resource
    @Output(Deadline.OUTPUT_ORDER)//消息发送通道指定自定义路口
    private MessageChannel output; // 消息发送管道

    public void send() {
        String serial = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serial).setHeader("x-delay",60*1000).build());
        System.out.println("发送消息: "+serial+new Date());
    }
    @StreamListener(Deadline.INPUT_ORDER)
    public void input(Message<String> message) {
        System.out.println("死信消费者，接收："+message.getPayload()+new Date());
        //throw new RuntimeException();
    }
}
