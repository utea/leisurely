package com.utea.mq.binder;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 自定义 发送通道和接收通道
 *
 */

public interface RouteChannel {
    String INPUT_CHANNEL = "inputChannel";
    String OUTPUT_CHANNEL = "outputChannel";

    @Input(INPUT_CHANNEL)
    SubscribableChannel inputChannel();

    @Output(OUTPUT_CHANNEL)
    MessageChannel outputChannel();
}
