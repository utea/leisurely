package com.utea.mq.ctrl;

import com.utea.mq.binder.DelaySend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestCtrl {
    @Autowired
    private DelaySend delaySend;
    @GetMapping("/send")
    public String send(){
        delaySend.send();
        return "success";
    }
}
