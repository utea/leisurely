package com.utea.task.mq;

import com.utea.pojo.task.TaskInfo;
import com.utea.task.dynamic.TaskRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;

@EnableBinding(TaskBinder.class)
public class TaskBinding {
    @Resource
    @Output(TaskBinder.OUTPUT_TASK)//消息发送通道指定自定义路口
    private MessageChannel output; // 消息发送管道

    @Autowired
    private TaskRegistrar taskRegistrar;

    public void send(TaskMsg taskMsg){
        System.out.println("消息发送"+taskMsg);
        output.send(MessageBuilder.withPayload(taskMsg).build());
    }
    @StreamListener(TaskBinder.INPUT_TASK)
    public void receive(Message<TaskMsg> message){
        System.out.println("消息处理"+message);
        switch (message.getPayload().getType()){
            case 1:{
                taskRegistrar.addCronTask(message.getPayload().getTaskInfo());
                break;
            }
            case 2:{
                taskRegistrar.removeCronTask(message.getPayload().getTaskInfo().getId());
                break;
            }
        }
    }
}
