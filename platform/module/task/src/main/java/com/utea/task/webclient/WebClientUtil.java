package com.utea.task.webclient;

import com.utea.pojo.base.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * webClient 支持
 */
@Service
public class WebClientUtil {
    @Autowired
    private WebClient.Builder clientBuilder;

    public Mono<JsonResult> post(String url,Object param){
        return clientBuilder
                .baseUrl("http://"+url)//指定url，ps:usr/adminUsr/findPage
                .build()
                .method(HttpMethod.POST)//post请求
                .bodyValue(param)//参数 Object类型
                .header("from", "task")//header 部分的内容
                .retrieve()//请求结果的方法
                .bodyToMono(JsonResult.class).retry(3);
    }
    public Mono<JsonResult> get(String url){
        return clientBuilder
                .baseUrl("http://"+url)//指定url，ps:usr/adminUsr/del?????
                .build()
                .method(HttpMethod.GET)//post请求
                .header("from", "task")//header 部分的内容
                .retrieve()//请求结果的方法
                .bodyToMono(JsonResult.class).retry(3);
    }
}
