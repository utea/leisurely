package com.utea.task.dao;

import com.utea.pojo.task.TaskInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author utea
 * @since 2020-11-26
 */
public interface TaskInfoDao extends BaseMapper<TaskInfo> {

}
