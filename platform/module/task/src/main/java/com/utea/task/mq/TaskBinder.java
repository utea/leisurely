package com.utea.task.mq;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface TaskBinder {
    String INPUT_TASK = "taskInput";
    String OUTPUT_TASK = "taskOutput";

    @Input(INPUT_TASK)
    SubscribableChannel taskInput();

    @Output(OUTPUT_TASK)
    MessageChannel taskOutput();
}
