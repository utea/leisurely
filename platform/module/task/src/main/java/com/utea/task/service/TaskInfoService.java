package com.utea.task.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.utea.common.constant.GlobalConstant;
import com.utea.common.snowflake.SnowfakeId;
import com.utea.common.utils.WrapperUtil;
import com.utea.pojo.base.Extend;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.task.TaskInfo;
import com.utea.pojo.usr.adminUsr.AdminUsr;
import com.utea.task.dao.TaskInfoDao;
import com.utea.task.dynamic.TaskRegistrar;
import com.utea.task.mq.TaskBinding;
import com.utea.task.mq.TaskMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author utea
 * @since 2020-11-26
 */
@Service
public class TaskInfoService extends ServiceImpl<TaskInfoDao, TaskInfo> {

    @Autowired
    private SnowfakeId snowfakeId;
    @Autowired
    private TaskRegistrar taskRegistrar;
    @Autowired
    private TaskBinding taskBinding;

    /*
    初始加载load
     */
    public void loadTask() {
        List<TaskInfo> taskInfos = list(WrapperUtil.queryWrapper(new TaskInfo().setStatus(1)));
        taskRegistrar.clear();
        taskInfos.forEach(taskRegistrar::addCronTask);
    }

    @Transactional
    public JsonResult<String> addTask(TaskInfo taskInfo) {
        taskInfo.setId(snowfakeId.id()).setCreateTime(DateUtil.now());
        baseMapper.insert(taskInfo);
        if(taskInfo.getStatus()== GlobalConstant.ACTIVE.UP)
            taskBinding.send(new TaskMsg().setType(1).setTaskInfo(taskInfo));
        return JsonResult.success("新增成功");
    }

    public JsonResult<IPage<TaskInfo>> findPage(TaskInfo taskInfo) {
        Extend extend = Optional.ofNullable(taskInfo).map(TaskInfo::getExtend).orElse(new Extend());
        Page<TaskInfo> page = new Page<TaskInfo>(extend.getCurrent(),extend.getSize())
                .setAsc(extend.getAsc()).setDesc(extend.getDesc());
        IPage<TaskInfo> result = getBaseMapper().selectPage(page, WrapperUtil.queryWrapper(taskInfo));
        return JsonResult.success(result);
    }

    @Transactional
    public JsonResult<String> updTask(TaskInfo taskInfo) {
        if(StrUtil.isEmpty(taskInfo.getId())){
            return new JsonResult<String>().error("id");
        }
        updateById(taskInfo);
        if(taskInfo.getStatus()== GlobalConstant.ACTIVE.UP)
            taskBinding.send(new TaskMsg().setType(1).setTaskInfo(taskInfo));
        else
            taskBinding.send(new TaskMsg().setType(2).setTaskInfo(taskInfo));
        return JsonResult.success("更新成功");
    }
    @Transactional
    public JsonResult<String> delTaskById(String taskId){
        removeById(taskId);
        taskBinding.send(new TaskMsg().setType(2).setTaskInfo(new TaskInfo().setId(taskId)));
        return JsonResult.success("删除成功");
    }
}
