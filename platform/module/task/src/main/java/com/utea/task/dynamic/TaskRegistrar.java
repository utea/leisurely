package com.utea.task.dynamic;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.utea.common.utils.RedisUtil;
import com.utea.pojo.task.TaskInfo;
import com.utea.task.webclient.WebClientUtil;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 任务维护服务
 */
@Service
public class TaskRegistrar{

    @Value("${task.server:0}")
    private int taskServer;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private WebClientUtil webClientUtil;
    //数量少 可以先存储在本地内存
    private final Map<String, ScheduledTask> scheduledTasks = new ConcurrentHashMap<>(16);


    private String getPrefix(){
        String TASK_KEY_PREFIX = "cron_task_";
        return TASK_KEY_PREFIX +taskServer;
    }

    public void addCronTask(TaskInfo taskInfo) {
        addCronTask(taskInfo.getId(),new CronTask(taskRun(taskInfo), taskInfo.getCron()));
    }

    private void addCronTask(String cronId,CronTask cronTask) {
        if (cronTask != null) {
            removeCronTask(cronId);
            //redisUtil.hset(getPrefix(),cronId,scheduleCronTask(cronTask));
            scheduledTasks.put(cronId,scheduleCronTask(cronTask));
        }
    }
    private ScheduledTask scheduleCronTask(CronTask cronTask) {
        ScheduledTask scheduledTask = new ScheduledTask();
        scheduledTask.future = taskScheduler.schedule(cronTask.getRunnable(), cronTask.getTrigger());
        return scheduledTask;
    }
    public void removeCronTask(String cronId) {
//        Optional.ofNullable(redisUtil.hget(getPrefix(),cronId)).map(m->(ScheduledTask)m)
//                .ifPresent(m->{
//                    m.cancel();
//                    redisUtil.hdel(getPrefix(),cronId);
//                });
        Optional.ofNullable(scheduledTasks.get(cronId))
                .ifPresent(m->{
                    m.cancel();
                    redisUtil.hdel(getPrefix(),cronId);
                });
    }
    public void clear(){
        redisUtil.del(getPrefix());
    }
    private Runnable taskRun(TaskInfo taskInfo){
        return ()->{
            RLock lock = redissonClient.getLock("task_lock_"+taskInfo.getId());
            try {
                if(lock.tryLock(0,30, TimeUnit.SECONDS)){
                    switch (taskInfo.getApiMethod()){
                        case 1:{
                            webClientUtil.get(taskInfo.getApi()).subscribe();
                            break;
                        }
                        case 2:{
                            webClientUtil.post(taskInfo.getApi(),Optional.ofNullable(taskInfo.getParam())
                                    .filter(StrUtil::isNotEmpty).map(JSONUtil::parseObj)
                                    .orElse(JSONUtil.parseObj("{}"))).subscribe();
                            break;
                        }
                    }
                    lock.unlock();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                lock.unlock();
            }
        };
    }
}
