package com.utea.task.ctrl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.task.TaskInfo;
import com.utea.task.service.TaskInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author utea
 * @since 2020-11-26
 */
@RestController
@RequestMapping("/taskInfo")
public class TaskInfoCtrl{
    @Autowired
    private TaskInfoService taskInfoService;

    @PostMapping("/addTask")
    public JsonResult<String> addTask(@RequestBody TaskInfo taskInfo){
        return taskInfoService.addTask(taskInfo);
    }
    @PostMapping("/findPage")
    public JsonResult<IPage<TaskInfo>> findPage(@RequestBody TaskInfo taskInfo){
        return taskInfoService.findPage(taskInfo);
    }
    @PostMapping("/updTask")
    public JsonResult<String> updTask(@RequestBody TaskInfo taskInfo){
        return taskInfoService.updTask(taskInfo);
    }
    @GetMapping("/delTask/{taskId}")
    public JsonResult<String> delTask(@PathVariable("taskId") String taskId){
        return taskInfoService.delTaskById(taskId);
    }
}

