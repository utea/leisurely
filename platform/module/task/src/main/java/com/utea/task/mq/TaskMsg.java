package com.utea.task.mq;

import com.utea.pojo.task.TaskInfo;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TaskMsg {
    private int type ;//1添加，2删除
    private TaskInfo taskInfo;
}
