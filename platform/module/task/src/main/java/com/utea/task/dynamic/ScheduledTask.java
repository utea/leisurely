package com.utea.task.dynamic;

import lombok.Data;

import java.util.concurrent.ScheduledFuture;

@Data
public class ScheduledTask {
    volatile ScheduledFuture<?> future;

    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.future;
        if (future != null) {
            future.cancel(true);
        }
    }
}
