package com.utea.task.runner;

import com.utea.task.service.TaskInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 启动
 */
@Component
public class ScheduleRunner implements CommandLineRunner {

    @Autowired
    private TaskInfoService taskInfoService;

    @Override
    public void run(String... args) throws Exception {
        taskInfoService.loadTask();
    }
}
