package com.utea.stomp.ctrl;

import com.utea.pojo.stomp.MessageBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/message")
public class MessageCtrl {
    @Autowired
    private SimpMessageSendingOperations simpMessageSendingOperations;

    @GetMapping("/sendCustom")
    public void sendCustom() {
        // 设置发送消息的用户
        MessageBody mb = new MessageBody();
        mb.setFrom("a").setTo("1").setContent("user").setType(1);
        // 调用 STOMP 代理进行消息转发
        simpMessageSendingOperations.convertAndSend("/queue/"+mb.getTo()+".message", mb);
    }
    @GetMapping("/sendBroadcast")
    public void sendBroadcast() {
        MessageBody mb = new MessageBody();
        mb.setFrom("a").setTo("b").setContent("broadcast").setType(1);
        simpMessageSendingOperations.convertAndSend("/topic/utea.broadcast",mb);
    }
    @GetMapping("/sendWeb")
    public void sendWeb() {
        MessageBody mb = new MessageBody();
        mb.setFrom("a").setTo("b").setContent("web").setType(1);
        simpMessageSendingOperations.convertAndSend("/queue/channel.web", mb);
    }
}
