package com.utea.stomp.conf.event;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.security.Principal;

@Log4j2
@Component
public class WsSessionSubscribeEvent implements ApplicationListener<SessionSubscribeEvent> {

    @Override
    public void onApplicationEvent(SessionSubscribeEvent event) {
        MessageHeaders headers = event.getMessage().getHeaders();
        String simpDestination = String.valueOf(headers.get("simpDestination"));
        Principal userPrincipal = event.getUser();
        log.info("{}订阅了{}",userPrincipal.getName(),simpDestination);
    }
}
