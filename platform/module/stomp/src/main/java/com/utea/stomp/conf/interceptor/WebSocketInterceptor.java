package com.utea.stomp.conf.interceptor;


import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.utea.common.global.token.TokenClaim;
import com.utea.common.utils.JwtUtils;
import com.utea.pojo.stomp.CustomPrincipal;
import com.utea.stomp.feign.usr.CheckFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Component;

/**
 * 这里做拦截判断权限
 * 因为 HandshakeInterceptor这边的headers 没法自定义，参数要放路径上传输
 */
@Component
public class WebSocketInterceptor implements ChannelInterceptor {

    @Autowired
    private CheckFeign checkFeign;
    /**
     * 从 Header 中获取 Token 进行验证，根据不同的 Token 区别用户
     *
     * @param message 消息对象
     * @param channel 通道对象
     * @return 验证后的用户信息
     */
    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        if (accessor != null && StompCommand.CONNECT.equals(accessor.getCommand())) {
            String token = accessor.getFirstNativeHeader("token");
            if(StrUtil.isNotEmpty(token) && JwtUtils.verifyToken(token)){
                TokenClaim tc = TokenClaim.instanceBean(JSONUtil.parseObj(JwtUtils.parseToken(token)));
                CustomPrincipal custom = new CustomPrincipal();
                custom.setId(tc.getId()).setUsername(tc.getUsername()).setType(0);
                // 提前创建好两个测试 token 进行匹配，方便测试
                accessor.setUser(custom);
            } else {
                message = null;
                channel.send(message);//无token 则直接返回空数据
            }
            return message;
        }
        return message;
    }
}
