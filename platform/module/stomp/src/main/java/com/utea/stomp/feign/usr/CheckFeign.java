package com.utea.stomp.feign.usr;

import com.utea.pojo.base.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "usr",path = "/",contextId = "checkFeign")
public interface CheckFeign {
    @GetMapping("/checkToken/{token}")
    JsonResult<String> checkToken(@PathVariable("token") String token);
}
