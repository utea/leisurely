package com.utea.stomp.conf.event;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

import java.security.Principal;

@Log4j2
@Component
public class WsSessionUnsubscribeEvent implements ApplicationListener<SessionUnsubscribeEvent> {
    @Override
    public void onApplicationEvent(SessionUnsubscribeEvent event) {
        MessageHeaders headers = event.getMessage().getHeaders();
        String simpDestination = String.valueOf(headers.get("simpDestination"));
        Principal userPrincipal = event.getUser();
        log.info("{}退订了{}",userPrincipal.getName(),simpDestination);
    }
}
