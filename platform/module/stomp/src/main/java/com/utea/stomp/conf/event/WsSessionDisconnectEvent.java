package com.utea.stomp.conf.event;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;

@Component
@Log4j2
public class WsSessionDisconnectEvent implements ApplicationListener<SessionDisconnectEvent> {
    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        Principal user = event.getUser();
        if(user==null){
            log.info("有人断开连接了");
        }else{
            log.info("{}断开连接了",user.getName());
        }
    }
}
