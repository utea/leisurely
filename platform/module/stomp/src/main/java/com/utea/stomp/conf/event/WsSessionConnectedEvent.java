package com.utea.stomp.conf.event;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

import java.security.Principal;


@Log4j2
@Component
public class WsSessionConnectedEvent implements ApplicationListener<SessionConnectedEvent> {

    /**
     * 事件类型可能有BrokerAvailabilityEvent,SessionConnectEvent,SessionConnectedEvent,SessionSubscribeEvent,SessionUnsubscribeEvent,SessionDisconnectEvent
     * https://docs.spring.io/spring/docs/5.1.7.RELEASE/spring-framework-reference/web.html#websocket-stomp-appplication-context-events
     * @param
     */

    @Override
    public void onApplicationEvent(SessionConnectedEvent event) {
        Principal user = event.getUser();
        if(user==null){
            log.info("有人连接了");
        }else{
            log.info("{}连接了",user.getName());
        }

    }
}
