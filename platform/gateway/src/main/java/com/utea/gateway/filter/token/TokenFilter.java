package com.utea.gateway.filter.token;

import cn.hutool.core.util.StrUtil;
import com.utea.gateway.feign.usr.CheckFeign;
import com.utea.gateway.filter.FilterKit;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.base.ResultEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Configuration
public class TokenFilter implements GlobalFilter, Ordered{
    private final static String HEAD_TOKEN = "token";
    @Autowired
    private CheckFeign checkFeign;
    @Autowired
    private TokenIgnorePath tokenIgnorePath;
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();
        if (FilterKit.isSwaggerUrl(path)||tokenIgnorePath.getPaths().stream().anyMatch(urlPath -> antPathMatcher.match(urlPath, path))) {
            return chain.filter(exchange);
        }
        final String token = request.getHeaders().getFirst(HEAD_TOKEN);
        if(token == null)
            return FilterKit.writeResponse(exchange,JsonResult.otherError(ResultEnum.NO_AUTH));
        return Mono.fromSupplier(()->checkFeign.checkToken(token))
                .flatMap(jr-> {
                    if(jr.getSuccess()){
                        if(jr.getData()!=null){
                            exchange.getResponse().getHeaders().add(HEAD_TOKEN, jr.getData());
                        }
                        return chain.filter(exchange);
                    }else {
                        return FilterKit.writeResponse(exchange,jr);
                    }
                });
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
