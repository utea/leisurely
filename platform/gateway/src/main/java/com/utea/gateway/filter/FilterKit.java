package com.utea.gateway.filter;

import cn.hutool.json.JSONUtil;
import com.utea.pojo.base.JsonResult;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class FilterKit {
    public static Mono<Void> writeResponse(ServerWebExchange exchange, JsonResult responseJson){
        ServerHttpResponse response = exchange.getResponse();
        //设置headers
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=1");
        //设置body
        DataBuffer bodyDataBuffer = response.bufferFactory().wrap(JSONUtil.toJsonStr(responseJson).getBytes());
        return exchange.getResponse().writeWith(Mono.just(bodyDataBuffer));
    }
    public static boolean isSwaggerUrl(String path){
        return path.contains("/swagger-resources/")||path.contains("/v2/api-docs")||path.contains("/swagger/api-docs")||path.contains("/webjars/");
    }
}
