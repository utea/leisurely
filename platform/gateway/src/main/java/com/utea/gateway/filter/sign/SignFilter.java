package com.utea.gateway.filter.sign;

import cn.hutool.core.date.DateUtil;
import com.utea.gateway.filter.FilterKit;
import com.utea.pojo.base.JsonResult;
import com.utea.pojo.base.ResultEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.rewrite.CachedBodyOutputMessage;
import org.springframework.cloud.gateway.support.BodyInserterContext;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;


/**
 * 进行签名校验
 * 这里只默认提供post json类型 和 get方式的签名校验
 * form表单和媒体文件的上传另外处理
 */
@Component
public class SignFilter implements GlobalFilter, Ordered {
    @Autowired
    private SignIgnorePath signIgnorePath;
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();
    @Value("${spring.profiles.active:dev}")
    private String active;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();
        //开发环境放行 knife4j的文件请求
        if ((active.equals("dev") && "Knife4j".equals(request.getHeaders().getFirst("Request-Origion")))
                || FilterKit.isSwaggerUrl(path) || signIgnorePath.getPaths().stream().anyMatch(urlPath -> antPathMatcher.match(urlPath, path))){
            return chain.filter(exchange);
        }
        //判断appId是否合规
        String appId = request.getHeaders().getFirst("appId");
        if(appId==null){
            return FilterKit.writeResponse(exchange, JsonResult.otherError(ResultEnum.NOT_SIGN));
        }
        //ApiAuthorize az = reactiveMongoTemplate.findOne(new Query(where("appId").is(appId)), ApiAuthorize.class).block();
        SignAuthorizeEnum az = SignAuthorizeEnum.getApiAuthorize(appId);
        if(az==null||az.getAppSecret()==null){
            return FilterKit.writeResponse(exchange, JsonResult.otherError(ResultEnum.NOT_SIGN));
        }
        SignedContent ac = new SignedContent().setAppId(appId).setAppSecret(az.getAppSecret())
                .setMethod(exchange.getRequest().getURI().getPath())
                .setUnique(Optional.ofNullable(request.getHeaders().getFirst("unique")).orElse(""))
                .setTimestamp(Long.parseLong(Optional.ofNullable(request.getHeaders().getFirst("timestamp")).orElse("0")));
        //判断有效
        if(ac.getTimestamp()+ac.getTimeoutExpress() < DateUtil.currentSeconds()){
            System.out.println("过期");
            return FilterKit.writeResponse(exchange, JsonResult.otherError(ResultEnum.NOT_SIGN));
        }
        Mono<String> modifiedBodyJson;
        //区分post和get
        if(exchange.getRequest().getMethodValue().toUpperCase().equals("GET")){
            if(!ac.getSign().equals(request.getHeaders().getFirst("sign"))){
                return FilterKit.writeResponse(exchange, JsonResult.otherError(ResultEnum.NOT_SIGN));
            }
            return chain.filter(exchange);
        } else if(exchange.getRequest().getMethodValue().toUpperCase().equals("POST")){
            ServerRequest serverRequest =ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders());
            // mediaType
            ac.setMediaType(exchange.getRequest().getHeaders().getContentType());
            //JSON请求
            modifiedBodyJson = serverRequest.bodyToMono(String.class)
                .flatMap(body -> {
                    //json类型
                    if (MediaType.APPLICATION_JSON.isCompatibleWith(ac.getMediaType())) {
                        //这边可以改变request body的值
                        ac.setReqContent(body);
                        return Mono.just(body);
                    }
                    return Mono.just(body);
                });
        } else {
            //其他类型 目前 不知道要不要拦截
            return chain.filter(exchange);
        }

        BodyInserter<Mono<String>, ReactiveHttpOutputMessage> bodyInserter =
                BodyInserters.fromPublisher(modifiedBodyJson, String.class);
        HttpHeaders headers = new HttpHeaders();
        headers.putAll(exchange.getRequest().getHeaders());

        // the new content type will be computed by bodyInserter
        // and then set in the request decorator
        headers.remove(HttpHeaders.CONTENT_LENGTH);

        CachedBodyOutputMessage outputMessage = new CachedBodyOutputMessage(exchange, headers);

        return bodyInserter.insert(outputMessage,  new BodyInserterContext())
                .then(Mono.defer(() -> {
                    if(!ac.postSign().equals(request.getHeaders().getFirst("sign"))){
                        //非法请求
                        return FilterKit.writeResponse(exchange, JsonResult.otherError(ResultEnum.NOT_SIGN));
                    }
                    ServerHttpRequestDecorator decorator = new ServerHttpRequestDecorator(
                            exchange.getRequest()) {
                        @Override
                        public HttpHeaders getHeaders() {
                            long contentLength = headers.getContentLength();
                            HttpHeaders httpHeaders = new HttpHeaders();
                            httpHeaders.putAll(super.getHeaders());
                            if (contentLength > 0) {
                                httpHeaders.setContentLength(contentLength);
                            } else {
                                httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
                            }
                            return httpHeaders;
                        }

                        @Override
                        public Flux<DataBuffer> getBody() {
                            return outputMessage.getBody();
                        }
                    };
                    return chain.filter(exchange.mutate().request(decorator).build());
                }));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
