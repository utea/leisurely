package com.utea.gateway.filter.sign;

import java.util.Arrays;
import java.util.Objects;

public enum SignAuthorizeEnum {
    AppV1("sign1","123i89wu0sduuihdfniosu981uo0ufj","app测试验证");
    private String appId;
    private String appSecret;
    private String authorizedRemark;


    SignAuthorizeEnum(String appId, String appSecret, String authorizedRemark) {
        this.appId = appId;
        this.appSecret = appSecret;
        this.authorizedRemark = authorizedRemark;
    }
    // 定义 get set 方法
    public String getAppSecret() {
        return appSecret;
    }

    public static SignAuthorizeEnum getApiAuthorize(String appId){
        return Arrays.stream(SignAuthorizeEnum.values()).filter(a-> Objects.equals(a.appId, appId)).findFirst().orElse(null);
    }
}
