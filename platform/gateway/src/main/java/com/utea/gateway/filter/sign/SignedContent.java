package com.utea.gateway.filter.sign;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.http.MediaType;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * API 签名内容
 * 该bean的内容用于和APPSecret进行加密签名
 */
@Data
@Accessors(chain = true)
public class SignedContent {
    //@ApiModelProperty("提供的appId")
    private String appId;
    //@ApiModelProperty("appId对应的密钥")
    private String appSecret;
    //@ApiModelProperty("请求内容json字符串")
    private String reqContent;
    //@ApiModelProperty("请求方法")
    private String method;
    //@ApiModelProperty("请求时间")
    private Long timestamp;
    //@ApiModelProperty("请求有效期，单位秒")
    private int timeoutExpress=60;
    //@ApiModelProperty("唯一标志")
    private String unique;
    //@ApiModelProperty("请求类型")
    private MediaType mediaType;

    public String postSign(){
        //System.out.println(this.appSecret + this.method + this.sortParams() + this.timestamp + this.timeoutExpress);
        return MD5.create().digestHex(this.appSecret + this.method + this.sortParams(this.reqContent) + this.timestamp + this.unique);
    }
    public String getSign(){
        //System.out.println(this.appSecret + this.method + this.timestamp + this.timeoutExpress);
        return MD5.create().digestHex(this.appSecret + this.method + this.timestamp + this.unique);
    }

    /**
     * 参数字符串 {"c":3,"a":"1","b":"2"} -> a=1&b=2&c=3
     * ①要进行正序排序②要去除空值字段
     * @return
     */
    public String sortParams(String reqContent){
        try {
            if(reqContent.startsWith("{")&&reqContent.endsWith("}"))
                return objectParams(JSONUtil.parseObj(reqContent)).stream().sorted().collect(Collectors.joining("&"));
            else if(reqContent.startsWith("[")&&reqContent.endsWith("]"))
                return arrayParams(JSONUtil.parseArray(reqContent)).stream().sorted().collect(Collectors.joining("&"));
            else
                return reqContent;
        }catch (Exception e){
            return reqContent;
        }
    }
    /**
     * Object参数排序
     * @param jsonObject
     * @return
     */
    public List<String> objectParams(JSONObject jsonObject){
        return jsonObject.entrySet().stream()
                .filter(e-> ObjectUtil.isNotNull(e.getValue())&& ObjectUtil.isNotEmpty(e.getValue()))
                .flatMap(e->{
                    //判断是否是对象
                    if (e.getValue() instanceof JSONObject) {
                        return objectParams((JSONObject) e.getValue()).stream();
                    } else if(e.getValue() instanceof JSONArray){
                        return arrayParams((JSONArray) e.getValue()).stream();
                    } else {
                        return Stream.of(e.getKey()+"="+e.getValue().toString());
                    }
                }).collect(Collectors.toList());
    }
    /**
     * Array参数排序
     * @param jsonArray
     * @return
     */
    public List<String> arrayParams(JSONArray jsonArray){
        return jsonArray.stream().filter(e-> ObjectUtil.isNotNull(e)&& ObjectUtil.isNotEmpty(e))
                .flatMap(e->{
                    //判断是否是对象
                    if (e instanceof JSONObject) {
                        return objectParams((JSONObject) e).stream();
                    } else if(e instanceof JSONArray){
                        return arrayParams((JSONArray) e).stream();
                    } else {
                        return Stream.of(e.toString());
                    }
                }).collect(Collectors.toList());
    }
}
