/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : nacos

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 03/12/2020 14:25:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (1, 'store.mode', 'SEATA_CONFIG', 'redis', '86a1b907d54bf7010394bf316e183e67', '2020-10-19 05:16:30', '2020-10-19 05:16:30', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', 'seata存储方式', NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (2, 'store.redis.host', 'SEATA_CONFIG', '192.168.100.81', '2a6a48a9f2b935f72c02163b33ad7de0', '2020-10-19 05:17:12', '2020-11-30 05:37:24', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', '', '', '', 'text', '');
INSERT INTO `config_info` VALUES (3, 'store.redis.port', 'SEATA_CONFIG', '6379', '92c3b916311a5517d9290576e3ea37ad', '2020-10-19 05:17:38', '2020-10-19 05:17:38', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (4, 'store.redis.password', 'SEATA_CONFIG', '123456', 'e10adc3949ba59abbe56e057f20f883e', '2020-10-19 05:18:06', '2020-10-19 05:34:58', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', '', '', '', 'text', '');
INSERT INTO `config_info` VALUES (5, 'store.redis.maxConn', 'SEATA_CONFIG', '10', 'd3d9446802a44259755d38e6d163e820', '2020-10-19 05:18:24', '2020-10-19 05:18:24', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (6, 'store.redis.minConn', 'SEATA_CONFIG', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2020-10-19 05:18:47', '2020-10-19 05:18:47', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (7, 'store.redis.database', 'SEATA_CONFIG', '0', 'cfcd208495d565ef66e7dff9f98764da', '2020-10-19 05:19:06', '2020-10-19 05:19:06', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (8, 'store.redis.queryLimit', 'SEATA_CONFIG', '100', 'f899139df5e1059396431415e770c6dd', '2020-10-19 05:19:55', '2020-10-19 05:19:55', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (11, 'mysql.yaml', 'SHARE_GROUP', 'spring:\r\n  datasource:\r\n    url: jdbc:mysql://192.168.100.81:3306/utea?characterEncoding=UTF-8&useSSL=false&allowMultiQueries=true\r\n    driver-class-name: com.mysql.cj.jdbc.Driver\r\n    username: root\r\n    password: 123456', 'e45d3ed3d18b303947ce2a5967b8d72a', '2020-10-19 06:51:27', '2020-11-27 08:33:16', NULL, '192.168.139.1', '', '', '数据库配置共享', '', '', 'text', '');
INSERT INTO `config_info` VALUES (17, 'service.vgroupMapping.utea_seate_tx_group', 'SEATA_CONFIG', 'default', 'c21f969b5f03d33d43e04f8f136e7682', '2020-10-19 09:13:43', '2020-10-19 09:13:43', NULL, '192.168.139.1', '', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (19, 'seata.yaml', 'SHARE_GROUP', 'seata:\r\n  application-id: ${spring.application.name}\r\n  tx-service-group: utea_seate_tx_group\r\n  enable-auto-data-source-proxy: true\r\n  config:\r\n    type: nacos\r\n    nacos:\r\n      namespace: 86e7b68a-245c-4e72-a5b9-edcd3e71fc07\r\n      serverAddr: ${nacos.server-addr}\r\n      group: SEATA_CONFIG\r\n      userName: ${nacos.username}\r\n      password: ${nacos.password}\r\n  registry:\r\n    type: nacos\r\n    nacos:\r\n      namespace: 86e7b68a-245c-4e72-a5b9-edcd3e71fc07\r\n      serverAddr: ${nacos.server-addr}\r\n      userName: ${nacos.username}\r\n      password: ${nacos.password}', '76c9634f40231f38495ef0a366ad3408', '2020-10-20 02:45:04', '2020-11-17 05:49:26', NULL, '192.168.139.1', '', '', 'seata统一client配置文件', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (22, 'sharding-m-s.yaml', 'SHARE_GROUP', '# 配置真实数据源\r\nspring:\r\n  shardingsphere:\r\n    datasource:\r\n      names: m,s\r\n      # 配置第 1 个数据源\r\n      m:\r\n        type: com.zaxxer.hikari.HikariDataSource\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        username: root\r\n        password: 123456\r\n        jdbc-url: jdbc:mysql://192.168.100.81:3306/utea?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true\r\n      # 配置第 2 个数据源\r\n      s:\r\n        type: com.zaxxer.hikari.HikariDataSource\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        username: root\r\n        password: 123456\r\n        jdbc-url: jdbc:mysql://192.168.100.81:3306/utea?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true\r\n    masterslave:\r\n      name: ms\r\n      master-data-source-name: m\r\n      slave-data-source-names: s\r\n    props:\r\n      sql:\r\n        show: true\r\n  main:\r\n    allow-bean-definition-overriding: true #设置为true，表示后发现的bean会覆盖之前相同名称的bean', '1682de98948d01691744dca041cf359b', '2020-10-20 03:10:30', '2020-11-27 08:33:31', NULL, '192.168.139.1', '', '', 'sharding主从分离', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (24, 'mybatis-plus.yaml', 'SHARE_GROUP', 'mybatis-plus:\r\n  mapper-locations: classpath:/mapper/**/*.xml\r\n  #实体扫描，多个package用逗号或者分号分隔\r\n  typeAliasesPackage: com.utea.pojo\r\n  global-config:\r\n    #主键类型  0:\"数据库ID自增\", 1:\"用户输入ID\",2:\"全局唯一ID (数字类型唯一ID)\", 3:\"全局唯一ID UUID\";\r\n    id-type: 1\r\n    #字段策略 0:\"忽略判断\",1:\"非 NULL 判断\"),2:\"非空判断\"\r\n    field-strategy: 2\r\n    #驼峰下划线转换\r\n    db-column-underline: true\r\n    #刷新mapper 调试神器\r\n    refresh-mapper: true\r\n    #数据库大写下划线转换\r\n    #capital-mode: true\r\n    #逻辑删除配置\r\n    logic-delete-value: 0\r\n    logic-not-delete-value: 1\r\n  configuration:\r\n    map-underscore-to-camel-case: true\r\n    cache-enabled: false\r\n    log-impl: org.apache.ibatis.logging.slf4j.Slf4jImpl', '8a2dbd6e8a59685c61861a13a817c23a', '2020-10-20 03:13:18', '2020-11-26 09:01:43', NULL, '192.168.139.1', '', '', 'mybatis-plus配置文件', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (33, 'redission-single.yaml', 'SHARE_GROUP', 'spring:\r\n    redis:\r\n        redisson:\r\n            config: |\r\n                singleServerConfig:\r\n                    idleConnectionTimeout: 10000\r\n                    connectTimeout: 10000\r\n                    timeout: 3000\r\n                    retryAttempts: 3\r\n                    retryInterval: 1500\r\n                    password: 123456\r\n                    subscriptionsPerConnection: 5\r\n                    clientName: null\r\n                    address: \"redis://127.0.0.1:6379\"\r\n                    subscriptionConnectionMinimumIdleSize: 1\r\n                    subscriptionConnectionPoolSize: 50\r\n                    connectionMinimumIdleSize: 24\r\n                    connectionPoolSize: 64\r\n                    database: 0\r\n                    dnsMonitoringInterval: 5000\r\n                threads: 16\r\n                nettyThreads: 32\r\n                transportMode: \"NIO\"', '24458b00f4549b0ca19074684b99901e', '2020-11-20 08:58:20', '2020-11-20 09:23:23', NULL, '192.168.139.1', '', '', '', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (38, 'redission-cluster.yaml', 'SHARE_GROUP', 'spring:\r\n    redis:\r\n        redisson:\r\n           config: |\r\n             clusterServersConfig:\r\n                idleConnectionTimeout: 10000\r\n                connectTimeout: 10000\r\n                timeout: 3000\r\n                retryAttempts: 3\r\n                retryInterval: 1500\r\n                failedSlaveReconnectionInterval: 3000\r\n                failedSlaveCheckInterval: 60000\r\n                password: 123456\r\n                subscriptionsPerConnection: 5\r\n                clientName: null\r\n                subscriptionConnectionMinimumIdleSize: 1\r\n                subscriptionConnectionPoolSize: 50\r\n                slaveConnectionMinimumIdleSize: 24\r\n                slaveConnectionPoolSize: 64\r\n                masterConnectionMinimumIdleSize: 24\r\n                masterConnectionPoolSize: 64\r\n                readMode: \"SLAVE\"\r\n                subscriptionMode: \"SLAVE\"\r\n                nodeAddresses:\r\n                    - \"redis://192.168.139.3:6379\"\r\n                    - \"redis://192.168.139.3:6378\"\r\n                    - \"redis://192.168.139.3:6377\"\r\n                scanInterval: 1000\r\n                pingConnectionInterval: 0\r\n                keepAlive: false\r\n                tcpNoDelay: false\r\n                threads: 16\r\n                nettyThreads: 32\r\n                transportMode: \"NIO\"', '3139e80dc585aeac49ef01f90960d5f8', '2020-11-20 09:27:47', '2020-11-20 09:27:47', NULL, '192.168.139.1', '', '', 'reids集群配置', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (41, 'usr-rules', 'SENTINEL_GROUP', '[\r\n    {\r\n        \"resource\": \"/checkToken/{token}\",\r\n        \"limitApp\": \"default\",\r\n        \"grade\": 1,\r\n        \"count\": 3,\r\n        \"strategy\": 0,\r\n        \"controlBehavior\": 0,\r\n        \"clusterMode\": false\r\n    }\r\n]', '60847fd951b3af4d4cfc415dd63aa8f9', '2020-11-24 03:37:47', '2020-11-24 03:37:47', NULL, '192.168.139.1', '', 'sentinel', '测试规则', NULL, NULL, 'json', NULL);
INSERT INTO `config_info` VALUES (42, 'gateway-rules', 'SENTINEL_GROUP', '[\r\n    {\r\n        \"resource\": \"/checkToken/{token}\",\r\n        \"limitApp\": \"default\",\r\n        \"grade\": 1,\r\n        \"count\": 3,\r\n        \"strategy\": 0,\r\n        \"controlBehavior\": 0,\r\n        \"clusterMode\": false\r\n    }\r\n]', '60847fd951b3af4d4cfc415dd63aa8f9', '2020-11-24 03:58:25', '2020-11-24 03:58:25', NULL, '192.168.139.1', '', 'sentinel', '测试规则', NULL, NULL, 'json', NULL);
INSERT INTO `config_info` VALUES (44, 'sentinel.yaml', 'SHARE_GROUP', 'spring:\r\n    cloud:\r\n        sentinel:\r\n            transport:\r\n                dashboard: 127.0.0.1:8998 #sentinel地址\r\n            datasource:\r\n                # 名称随意\r\n                javatrip:\r\n                nacos:\r\n                    server-addr: ${nacos.server-addr}\r\n                    username: ${nacos.username}\r\n                    password: ${nacos.password}\r\n                    dataId: ${spring.application.name}-rules\r\n                    namespace: sentinel #命名空间id\r\n                    groupId: SENTINEL_GROUP\r\n                    # 规则类型，取值见：\r\n                    # org.springframework.cloud.alibaba.sentinel.datasource.RuleType\r\n                    rule-type: flow  \r\n#开启feign支持                    \r\nfeign:\r\n    sentinel:\r\n        enabled: true                    ', 'c93e0ce65860c28be2e25905a21007e1', '2020-11-24 04:02:17', '2020-11-24 04:02:17', NULL, '192.168.139.1', '', '', 'sentinel配置', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (46, 'rabbitmq.yaml', 'SHARE_GROUP', 'spring:\r\n    rabbitmq:\r\n        host: localhost\r\n        port: 5672\r\n        stomp: 61613\r\n        username: admin\r\n        password: 123456', '447220b4a17636ec7c3ed291a6367154', '2020-11-25 01:35:43', '2020-11-25 01:43:59', NULL, '192.168.139.1', '', '', 'rabbitmq配置', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (48, 'zipkin.yaml', 'SHARE_GROUP', 'spring:\r\n    sleuth:\r\n        sampler:\r\n            probability: 1\r\n    zipkin:\r\n        base-url: http://localhost:9411\r\n        sender:\r\n            type: rabbit', 'ee133c4612b27781af40fee60ba3c100', '2020-11-25 02:17:17', '2020-11-25 02:17:17', NULL, '192.168.139.1', '', '', 'zipkin 配置', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (53, 'mongodb.yaml', 'SHARE_GROUP', 'spring:\r\n  data:\r\n    mongodb: \r\n      uri: mongodb://utea:123456@192.168.139.6:27017,192.168.139.6:27018,192.168.139.6:27019/utea\r\n      #uri: mongodb://utea:123456@192.168.139.6:27017/utea\r\nlogging:\r\n  level:\r\n    org:\r\n      springframework:\r\n        data:\r\n          mongodb:\r\n            core: DEBUG      ', 'b5e6bea30fd752b2f5009073e79e4ea4', '2020-12-02 08:24:45', '2020-12-03 06:24:15', NULL, '192.168.139.1', '', '', 'mongodb配置\n#username和password中含有“:”或“@”需要进行URLEncoder编码', '', '', 'yaml', '');

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_aggr
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(64) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (19, 35, 'seata.yaml', 'SHARE_GROUP', '', 'seata:\r\n  application-id: ${spring.application.name}\r\n  tx-service-group: utea_seate_tx_group\r\n  enable-auto-data-source-proxy: false\r\n  config:\r\n    type: nacos\r\n    nacos:\r\n      namespace: 86e7b68a-245c-4e72-a5b9-edcd3e71fc07\r\n      serverAddr: ${nacos.server-addr}\r\n      group: SEATA_CONFIG\r\n      userName: ${nacos.username}\r\n      password: ${nacos.password}\r\n  registry:\r\n    type: nacos\r\n    nacos:\r\n      namespace: 86e7b68a-245c-4e72-a5b9-edcd3e71fc07\r\n      serverAddr: ${nacos.server-addr}\r\n      userName: ${nacos.username}\r\n      password: ${nacos.password}', '28ba8b621b8d26af8b4e5503880da763', '2020-11-17 13:49:23', '2020-11-17 05:49:26', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 36, 'rabbit.yaml', 'SHARE_GROUP', '', 'rabbitmq:\r\n    host: 192.168.0.99\r\n    port: 5672\r\n    username: admin\r\n    password: 123456', '28c751a25f2b674245915de7e2c386e7', '2020-11-19 11:04:22', '2020-11-19 03:04:25', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (28, 37, 'rabbit.yaml', 'SHARE_GROUP', '', 'rabbitmq:\r\n    host: 192.168.0.99\r\n    port: 5672\r\n    username: admin\r\n    password: 123456', '28c751a25f2b674245915de7e2c386e7', '2020-11-19 11:44:40', '2020-11-19 03:44:43', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (28, 38, 'rabbit.yaml', 'SHARE_GROUP', '', 'rabbitmq:\r\n    host: 192.168.0.99\r\n    port: 5672\r\n    username: admin\r\n    password: 1234567', '4a66617fa0bc3e34bb92bad26c77eb6f', '2020-11-19 11:45:13', '2020-11-19 03:45:16', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (28, 39, 'rabbit.yaml', 'SHARE_GROUP', '', 'rabbitmq:\r\n    host: 192.168.0.99\r\n    port: 5672\r\n    username: admin\r\n    password: 123456', '28c751a25f2b674245915de7e2c386e7', '2020-11-19 16:25:18', '2020-11-19 08:25:22', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 40, 'redission-single', 'SHARE_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n            config:\r\n                singleServerConfig:\r\n                    idleConnectionTimeout: 10000\r\n                    connectTimeout: 10000\r\n                    timeout: 3000\r\n                    retryAttempts: 3\r\n                    retryInterval: 1500\r\n                    password: 123456\r\n                    subscriptionsPerConnection: 5\r\n                    clientName: null\r\n                    address: \"redis://127.0.0.1:6379\"\r\n                    subscriptionConnectionMinimumIdleSize: 1\r\n                    subscriptionConnectionPoolSize: 50\r\n                    connectionMinimumIdleSize: 24\r\n                    connectionPoolSize: 64\r\n                    database: 0\r\n                    dnsMonitoringInterval: 5000\r\n                threads: 16\r\n                nettyThreads: 32\r\n                codec: !<org.redisson.codec.FstCodec> {}\r\n                transportMode: \"NIO\"', '70eb146b01422b2c568632304cab6409', '2020-11-20 16:51:06', '2020-11-20 08:51:10', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 41, 'redission-single.yaml', 'SHARE_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n            config:\r\n                singleServerConfig:\r\n                    idleConnectionTimeout: 10000\r\n                    connectTimeout: 10000\r\n                    timeout: 3000\r\n                    retryAttempts: 3\r\n                    retryInterval: 1500\r\n                    password: 123456\r\n                    subscriptionsPerConnection: 5\r\n                    clientName: null\r\n                    address: \"redis://127.0.0.1:6379\"\r\n                    subscriptionConnectionMinimumIdleSize: 1\r\n                    subscriptionConnectionPoolSize: 50\r\n                    connectionMinimumIdleSize: 24\r\n                    connectionPoolSize: 64\r\n                    database: 0\r\n                    dnsMonitoringInterval: 5000\r\n                threads: 16\r\n                nettyThreads: 32\r\n                codec: !<org.redisson.codec.FstCodec> {}\r\n                transportMode: \"NIO\"', '70eb146b01422b2c568632304cab6409', '2020-11-20 16:58:16', '2020-11-20 08:58:20', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (32, 42, 'redission-single', 'SHARE_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n            config:\r\n                singleServerConfig:\r\n                    idleConnectionTimeout: 10000\r\n                    connectTimeout: 10000\r\n                    timeout: 3000\r\n                    retryAttempts: 3\r\n                    retryInterval: 1500\r\n                    password: 123456\r\n                    subscriptionsPerConnection: 5\r\n                    clientName: null\r\n                    address: \"redis://127.0.0.1:6379\"\r\n                    subscriptionConnectionMinimumIdleSize: 1\r\n                    subscriptionConnectionPoolSize: 50\r\n                    connectionMinimumIdleSize: 24\r\n                    connectionPoolSize: 64\r\n                    database: 0\r\n                    dnsMonitoringInterval: 5000\r\n                threads: 16\r\n                nettyThreads: 32\r\n                codec: !<org.redisson.codec.FstCodec> {}\r\n                transportMode: \"NIO\"', '70eb146b01422b2c568632304cab6409', '2020-11-20 16:58:23', '2020-11-20 08:58:27', NULL, '192.168.139.1', 'D', '');
INSERT INTO `his_config_info` VALUES (33, 43, 'redission-single.yaml', 'SHARE_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n            config:\r\n                singleServerConfig:\r\n                    idleConnectionTimeout: 10000\r\n                    connectTimeout: 10000\r\n                    timeout: 3000\r\n                    retryAttempts: 3\r\n                    retryInterval: 1500\r\n                    password: 123456\r\n                    subscriptionsPerConnection: 5\r\n                    clientName: null\r\n                    address: \"redis://127.0.0.1:6379\"\r\n                    subscriptionConnectionMinimumIdleSize: 1\r\n                    subscriptionConnectionPoolSize: 50\r\n                    connectionMinimumIdleSize: 24\r\n                    connectionPoolSize: 64\r\n                    database: 0\r\n                    dnsMonitoringInterval: 5000\r\n                threads: 16\r\n                nettyThreads: 32\r\n                codec: !<org.redisson.codec.FstCodec> {}\r\n                transportMode: \"NIO\"', '70eb146b01422b2c568632304cab6409', '2020-11-20 17:00:20', '2020-11-20 09:00:24', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (33, 44, 'redission-single.yaml', 'SHARE_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n            config:\r\n                singleServerConfig:\r\n                    idleConnectionTimeout: 10000\r\n                    connectTimeout: 10000\r\n                    timeout: 3000\r\n                    retryAttempts: 3\r\n                    retryInterval: 1500\r\n                    password: 123456\r\n                    subscriptionsPerConnection: 5\r\n                    clientName: null\r\n                    address: \"redis://127.0.0.1:6379\"\r\n                    subscriptionConnectionMinimumIdleSize: 1\r\n                    subscriptionConnectionPoolSize: 50\r\n                    connectionMinimumIdleSize: 24\r\n                    connectionPoolSize: 64\r\n                    database: 0\r\n                    dnsMonitoringInterval: 5000\r\n                threads: 16\r\n                nettyThreads: 32\r\n                transportMode: \"NIO\"', 'a2af12630c9b3295d81a64bd9c217f8f', '2020-11-20 17:13:35', '2020-11-20 09:13:39', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (33, 45, 'redission-single.yaml', 'SHARE_GROUP', '', 'singleServerConfig:\r\n    idleConnectionTimeout: 10000\r\n    connectTimeout: 10000\r\n    timeout: 3000\r\n    retryAttempts: 3\r\n    retryInterval: 1500\r\n    password: 123456\r\n    subscriptionsPerConnection: 5\r\n    clientName: null\r\n    address: \"redis://127.0.0.1:6379\"\r\n    subscriptionConnectionMinimumIdleSize: 1\r\n    subscriptionConnectionPoolSize: 50\r\n    connectionMinimumIdleSize: 24\r\n    connectionPoolSize: 64\r\n    database: 0\r\n    dnsMonitoringInterval: 5000\r\nthreads: 16\r\nnettyThreads: 32\r\ntransportMode: \"NIO\"', '593d2b09af9c2e7d2deaa46ea229968c', '2020-11-20 17:23:19', '2020-11-20 09:23:23', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 46, 'redission-cluster.yaml', 'DEFAULT_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n           config: |\r\n             clusterServersConfig:\r\n                idleConnectionTimeout: 10000\r\n                connectTimeout: 10000\r\n                timeout: 3000\r\n                retryAttempts: 3\r\n                retryInterval: 1500\r\n                failedSlaveReconnectionInterval: 3000\r\n                failedSlaveCheckInterval: 60000\r\n                password: 123456\r\n                subscriptionsPerConnection: 5\r\n                clientName: null\r\n                subscriptionConnectionMinimumIdleSize: 1\r\n                subscriptionConnectionPoolSize: 50\r\n                slaveConnectionMinimumIdleSize: 24\r\n                slaveConnectionPoolSize: 64\r\n                masterConnectionMinimumIdleSize: 24\r\n                masterConnectionPoolSize: 64\r\n                readMode: \"SLAVE\"\r\n                subscriptionMode: \"SLAVE\"\r\n                nodeAddresses:\r\n                    - \"redis://192.168.139.3:6379\"\r\n                    - \"redis://192.168.139.3:6378\"\r\n                    - \"redis://192.168.139.3:6377\"\r\n                scanInterval: 1000\r\n                pingConnectionInterval: 0\r\n                keepAlive: false\r\n                tcpNoDelay: false\r\n                threads: 16\r\n                nettyThreads: 32\r\n                transportMode: \"NIO\"', '3139e80dc585aeac49ef01f90960d5f8', '2020-11-20 17:27:27', '2020-11-20 09:27:31', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 47, 'redission-cluster.yaml', 'SHARE_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n           config: |\r\n             clusterServersConfig:\r\n                idleConnectionTimeout: 10000\r\n                connectTimeout: 10000\r\n                timeout: 3000\r\n                retryAttempts: 3\r\n                retryInterval: 1500\r\n                failedSlaveReconnectionInterval: 3000\r\n                failedSlaveCheckInterval: 60000\r\n                password: 123456\r\n                subscriptionsPerConnection: 5\r\n                clientName: null\r\n                subscriptionConnectionMinimumIdleSize: 1\r\n                subscriptionConnectionPoolSize: 50\r\n                slaveConnectionMinimumIdleSize: 24\r\n                slaveConnectionPoolSize: 64\r\n                masterConnectionMinimumIdleSize: 24\r\n                masterConnectionPoolSize: 64\r\n                readMode: \"SLAVE\"\r\n                subscriptionMode: \"SLAVE\"\r\n                nodeAddresses:\r\n                    - \"redis://192.168.139.3:6379\"\r\n                    - \"redis://192.168.139.3:6378\"\r\n                    - \"redis://192.168.139.3:6377\"\r\n                scanInterval: 1000\r\n                pingConnectionInterval: 0\r\n                keepAlive: false\r\n                tcpNoDelay: false\r\n                threads: 16\r\n                nettyThreads: 32\r\n                transportMode: \"NIO\"', '3139e80dc585aeac49ef01f90960d5f8', '2020-11-20 17:27:43', '2020-11-20 09:27:47', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (37, 48, 'redission-cluster.yaml', 'DEFAULT_GROUP', '', 'spring:\r\n    redis:\r\n        redisson:\r\n           config: |\r\n             clusterServersConfig:\r\n                idleConnectionTimeout: 10000\r\n                connectTimeout: 10000\r\n                timeout: 3000\r\n                retryAttempts: 3\r\n                retryInterval: 1500\r\n                failedSlaveReconnectionInterval: 3000\r\n                failedSlaveCheckInterval: 60000\r\n                password: 123456\r\n                subscriptionsPerConnection: 5\r\n                clientName: null\r\n                subscriptionConnectionMinimumIdleSize: 1\r\n                subscriptionConnectionPoolSize: 50\r\n                slaveConnectionMinimumIdleSize: 24\r\n                slaveConnectionPoolSize: 64\r\n                masterConnectionMinimumIdleSize: 24\r\n                masterConnectionPoolSize: 64\r\n                readMode: \"SLAVE\"\r\n                subscriptionMode: \"SLAVE\"\r\n                nodeAddresses:\r\n                    - \"redis://192.168.139.3:6379\"\r\n                    - \"redis://192.168.139.3:6378\"\r\n                    - \"redis://192.168.139.3:6377\"\r\n                scanInterval: 1000\r\n                pingConnectionInterval: 0\r\n                keepAlive: false\r\n                tcpNoDelay: false\r\n                threads: 16\r\n                nettyThreads: 32\r\n                transportMode: \"NIO\"', '3139e80dc585aeac49ef01f90960d5f8', '2020-11-20 17:27:50', '2020-11-20 09:27:54', NULL, '192.168.139.1', 'D', '');
INSERT INTO `his_config_info` VALUES (11, 49, 'mysql.yaml', 'SHARE_GROUP', '', 'spring:\r\n  datasource:\r\n    url: jdbc:mysql://192.168.0.99:3306/utea?characterEncoding=UTF-8&useSSL=false&allowMultiQueries=true\r\n    driver-class-name: com.mysql.cj.jdbc.Driver\r\n    username: root\r\n    password: 123456', '0bd0f55c98b445b4ae471c7b5cde6289', '2020-11-24 09:33:26', '2020-11-24 01:33:31', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (22, 50, 'sharding-m-s.yaml', 'SHARE_GROUP', '', '# 配置真实数据源\r\nspring:\r\n  shardingsphere:\r\n    datasource:\r\n      names: m,s\r\n      # 配置第 1 个数据源\r\n      m:\r\n        type: com.zaxxer.hikari.HikariDataSource\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        username: root\r\n        password: 123456\r\n        jdbc-url: jdbc:mysql://192.168.0.88:3306/utea?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true\r\n      # 配置第 2 个数据源\r\n      s:\r\n        type: com.zaxxer.hikari.HikariDataSource\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        username: root\r\n        password: 123456\r\n        jdbc-url: jdbc:mysql://192.168.0.88:3306/utea?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true\r\n    masterslave:\r\n      name: ms\r\n      master-data-source-name: m\r\n      slave-data-source-names: s\r\n    props:\r\n      sql:\r\n        show: true\r\n  main:\r\n    allow-bean-definition-overriding: true #设置为true，表示后发现的bean会覆盖之前相同名称的bean', '5ed1542bbe463350c72609d52e00a1f3', '2020-11-24 09:33:48', '2020-11-24 01:33:54', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 51, 'usr-rules', 'SENTINEL_GROUP', '', '[\r\n    {\r\n        \"resource\": \"/checkToken/{token}\",\r\n        \"limitApp\": \"default\",\r\n        \"grade\": 1,\r\n        \"count\": 3,\r\n        \"strategy\": 0,\r\n        \"controlBehavior\": 0,\r\n        \"clusterMode\": false\r\n    }\r\n]', '60847fd951b3af4d4cfc415dd63aa8f9', '2020-11-24 11:37:41', '2020-11-24 03:37:47', NULL, '192.168.139.1', 'I', 'sentinel');
INSERT INTO `his_config_info` VALUES (0, 52, 'gateway-rules', 'SENTINEL_GROUP', '', '[\r\n    {\r\n        \"resource\": \"/checkToken/{token}\",\r\n        \"limitApp\": \"default\",\r\n        \"grade\": 1,\r\n        \"count\": 3,\r\n        \"strategy\": 0,\r\n        \"controlBehavior\": 0,\r\n        \"clusterMode\": false\r\n    }\r\n]', '60847fd951b3af4d4cfc415dd63aa8f9', '2020-11-24 11:58:19', '2020-11-24 03:58:25', NULL, '192.168.139.1', 'I', 'sentinel');
INSERT INTO `his_config_info` VALUES (0, 53, 'sentinel.yaml', 'DEFAULT_GROUP', '', 'spring:\r\n    cloud:\r\n        sentinel:\r\n            transport:\r\n                dashboard: 127.0.0.1:8998 #sentinel地址\r\n            datasource:\r\n                # 名称随意\r\n                javatrip:\r\n                nacos:\r\n                    server-addr: ${nacos.server-addr}\r\n                    username: ${nacos.username}\r\n                    password: ${nacos.password}\r\n                    dataId: ${spring.application.name}-rules\r\n                    namespace: sentinel #命名空间id\r\n                    groupId: SENTINEL_GROUP\r\n                    # 规则类型，取值见：\r\n                    # org.springframework.cloud.alibaba.sentinel.datasource.RuleType\r\n                    rule-type: flow  \r\n#开启feign支持                    \r\nfeign:\r\n    sentinel:\r\n        enabled: true                    ', 'c93e0ce65860c28be2e25905a21007e1', '2020-11-24 12:01:59', '2020-11-24 04:02:04', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 54, 'sentinel.yaml', 'SHARE_GROUP', '', 'spring:\r\n    cloud:\r\n        sentinel:\r\n            transport:\r\n                dashboard: 127.0.0.1:8998 #sentinel地址\r\n            datasource:\r\n                # 名称随意\r\n                javatrip:\r\n                nacos:\r\n                    server-addr: ${nacos.server-addr}\r\n                    username: ${nacos.username}\r\n                    password: ${nacos.password}\r\n                    dataId: ${spring.application.name}-rules\r\n                    namespace: sentinel #命名空间id\r\n                    groupId: SENTINEL_GROUP\r\n                    # 规则类型，取值见：\r\n                    # org.springframework.cloud.alibaba.sentinel.datasource.RuleType\r\n                    rule-type: flow  \r\n#开启feign支持                    \r\nfeign:\r\n    sentinel:\r\n        enabled: true                    ', 'c93e0ce65860c28be2e25905a21007e1', '2020-11-24 12:02:12', '2020-11-24 04:02:17', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (43, 55, 'sentinel.yaml', 'DEFAULT_GROUP', '', 'spring:\r\n    cloud:\r\n        sentinel:\r\n            transport:\r\n                dashboard: 127.0.0.1:8998 #sentinel地址\r\n            datasource:\r\n                # 名称随意\r\n                javatrip:\r\n                nacos:\r\n                    server-addr: ${nacos.server-addr}\r\n                    username: ${nacos.username}\r\n                    password: ${nacos.password}\r\n                    dataId: ${spring.application.name}-rules\r\n                    namespace: sentinel #命名空间id\r\n                    groupId: SENTINEL_GROUP\r\n                    # 规则类型，取值见：\r\n                    # org.springframework.cloud.alibaba.sentinel.datasource.RuleType\r\n                    rule-type: flow  \r\n#开启feign支持                    \r\nfeign:\r\n    sentinel:\r\n        enabled: true                    ', 'c93e0ce65860c28be2e25905a21007e1', '2020-11-24 12:02:18', '2020-11-24 04:02:24', NULL, '192.168.139.1', 'D', '');
INSERT INTO `his_config_info` VALUES (28, 56, 'rabbit.yaml', 'SHARE_GROUP', '', 'rabbitmq:\r\n    host: 192.168.0.99\r\n    port: 5672\r\n    stomp: 61613\r\n    username: admin\r\n    password: 123456', '5b58f21c60519fb4311fb1441e9ee1eb', '2020-11-25 09:33:20', '2020-11-25 01:33:21', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 57, 'rabbitmq.yaml', 'SHARE_GROUP', '', 'spring:\r\n    rabbitmq:\r\n        host: 192.168.0.99\r\n        port: 5672\r\n        stomp: 61613\r\n        username: admin\r\n        password: 123456', '775836d0a4ec7dbaa33d27f7fe7e4184', '2020-11-25 09:35:42', '2020-11-25 01:35:43', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (28, 58, 'rabbit.yaml', 'SHARE_GROUP', '', 'spring:\r\n    rabbitmq:\r\n        host: 192.168.0.99\r\n        port: 5672\r\n        stomp: 61613\r\n        username: admin\r\n        password: 123456', '775836d0a4ec7dbaa33d27f7fe7e4184', '2020-11-25 09:35:57', '2020-11-25 01:35:57', NULL, '192.168.139.1', 'D', '');
INSERT INTO `his_config_info` VALUES (46, 59, 'rabbitmq.yaml', 'SHARE_GROUP', '', 'spring:\r\n    rabbitmq:\r\n        host: 192.168.0.99\r\n        port: 5672\r\n        stomp: 61613\r\n        username: admin\r\n        password: 123456', '775836d0a4ec7dbaa33d27f7fe7e4184', '2020-11-25 09:43:58', '2020-11-25 01:43:59', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 60, 'zipkin.yaml', 'SHARE_GROUP', '', 'spring:\r\n    sleuth:\r\n        sampler:\r\n            probability: 1\r\n    zipkin:\r\n        base-url: http://localhost:9411\r\n        sender:\r\n            type: rabbit', 'ee133c4612b27781af40fee60ba3c100', '2020-11-25 10:17:17', '2020-11-25 02:17:17', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (24, 61, 'mybatis-plus.yaml', 'SHARE_GROUP', '', 'mybatis-plus:\r\n  mapper-locations: classpath:/mapper/**/*.xml\r\n  #实体扫描，多个package用逗号或者分号分隔\r\n  typeAliasesPackage: com.utea.pojo\r\n  global-config:\r\n    #主键类型  0:\"数据库ID自增\", 1:\"用户输入ID\",2:\"全局唯一ID (数字类型唯一ID)\", 3:\"全局唯一ID UUID\";\r\n    id-type: 1\r\n    #字段策略 0:\"忽略判断\",1:\"非 NULL 判断\"),2:\"非空判断\"\r\n    field-strategy: 2\r\n    #驼峰下划线转换\r\n    db-column-underline: true\r\n    #刷新mapper 调试神器\r\n    refresh-mapper: true\r\n    #数据库大写下划线转换\r\n    #capital-mode: true\r\n    #逻辑删除配置\r\n    logic-delete-value: 0\r\n    logic-not-delete-value: 1\r\n  configuration:\r\n    map-underscore-to-camel-case: true\r\n    cache-enabled: false', 'e5ec6a592ed29d8e70b6a6bc0f1d6b26', '2020-11-26 17:01:42', '2020-11-26 09:01:43', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (11, 62, 'mysql.yaml', 'SHARE_GROUP', '', 'spring:\r\n  datasource:\r\n    url: jdbc:mysql://10.6.72.47:3306/utea?characterEncoding=UTF-8&useSSL=false&allowMultiQueries=true\r\n    driver-class-name: com.mysql.cj.jdbc.Driver\r\n    username: root\r\n    password: 123456', 'a5e6f0c9321cf7b8c1828b156a37f73f', '2020-11-27 16:33:16', '2020-11-27 08:33:16', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (22, 63, 'sharding-m-s.yaml', 'SHARE_GROUP', '', '# 配置真实数据源\r\nspring:\r\n  shardingsphere:\r\n    datasource:\r\n      names: m,s\r\n      # 配置第 1 个数据源\r\n      m:\r\n        type: com.zaxxer.hikari.HikariDataSource\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        username: root\r\n        password: 123456\r\n        jdbc-url: jdbc:mysql://10.6.72.47:3306/utea?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true\r\n      # 配置第 2 个数据源\r\n      s:\r\n        type: com.zaxxer.hikari.HikariDataSource\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        username: root\r\n        password: 123456\r\n        jdbc-url: jdbc:mysql://10.6.72.47:3306/utea?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true\r\n    masterslave:\r\n      name: ms\r\n      master-data-source-name: m\r\n      slave-data-source-names: s\r\n    props:\r\n      sql:\r\n        show: true\r\n  main:\r\n    allow-bean-definition-overriding: true #设置为true，表示后发现的bean会覆盖之前相同名称的bean', '149b2b34198f6562830939dc349cb34b', '2020-11-27 16:33:30', '2020-11-27 08:33:31', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (2, 64, 'store.redis.host', 'SEATA_CONFIG', '', '192.168.0.99', '5d8ea6a2e7766da436676df5f0bd1534', '2020-11-30 13:37:24', '2020-11-30 05:37:24', NULL, '192.168.139.1', 'U', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07');
INSERT INTO `his_config_info` VALUES (0, 65, 'mongodb.yaml', 'SHARE_GROUP', '', 'spring:\r\n  data:\r\n    mongodb: \r\n      #uri: mongodb://test_user:pass%40123@192.168.139.6:27017,192.168.1.2:27017,192.168.1.3:27017/utea\r\n      uri: mongodb://root:root@192.168.139.6:27017/utea', '3d95cdf502c203ea8506cbf285db9001', '2020-12-02 16:24:44', '2020-12-02 08:24:45', NULL, '192.168.139.1', 'I', '');
INSERT INTO `his_config_info` VALUES (53, 66, 'mongodb.yaml', 'SHARE_GROUP', '', 'spring:\r\n  data:\r\n    mongodb: \r\n      #uri: mongodb://test_user:pass%40123@192.168.139.6:27017,192.168.1.2:27017,192.168.1.3:27017/utea\r\n      uri: mongodb://root:root@192.168.139.6:27017/utea', '3d95cdf502c203ea8506cbf285db9001', '2020-12-02 17:39:59', '2020-12-02 09:40:00', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (53, 67, 'mongodb.yaml', 'SHARE_GROUP', '', 'spring:\r\n  data:\r\n    mongodb: \r\n      #uri: mongodb://test_user:pass%40123@192.168.139.6:27017,192.168.1.2:27017,192.168.1.3:27017/utea\r\n      uri: mongodb://utea:123456@192.168.139.6:27017/utea', '43c5e7d40746172e24bc4e2fbe194805', '2020-12-03 13:14:55', '2020-12-03 05:14:56', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (53, 68, 'mongodb.yaml', 'SHARE_GROUP', '', 'spring:\r\n  data:\r\n    mongodb: \r\n      uri: mongodb://test_user:pass%40123@192.168.139.6:27017,192.168.139.6:27018,192.168.139.6:27019/utea\r\n      #uri: mongodb://utea:123456@192.168.139.6:27017/utea', '1657488e2811169d56b4287409f42e0a', '2020-12-03 13:16:43', '2020-12-03 05:16:44', NULL, '192.168.139.1', 'U', '');
INSERT INTO `his_config_info` VALUES (53, 69, 'mongodb.yaml', 'SHARE_GROUP', '', 'spring:\r\n  data:\r\n    mongodb: \r\n      uri: mongodb://utea:123456@192.168.139.6:27017,192.168.139.6:27018,192.168.139.6:27019/utea\r\n      #uri: mongodb://utea:123456@192.168.139.6:27017/utea', '987eaa1887cf788c6a13efe1d9bf61d9', '2020-12-03 14:24:14', '2020-12-03 06:24:15', NULL, '192.168.139.1', 'U', '');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `resource` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `action` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  UNIQUE INDEX `uk_role_permission`(`role`, `resource`, `action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  UNIQUE INDEX `idx_user_role`(`username`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES (1, '1', '86e7b68a-245c-4e72-a5b9-edcd3e71fc07', 'seata', 'seata注册和配置相关', 'nacos', 1603076017578, 1603076017578);
INSERT INTO `tenant_info` VALUES (2, '1', 'sentinel', 'sentinel', 'sentinel配置', 'nacos', 1606188714869, 1606188714869);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$nqewKKq/so7ymAJeR3ZAQui12DUJAwk2Qn0NPgxQMPgzS1zoxKle6', 1);

SET FOREIGN_KEY_CHECKS = 1;
