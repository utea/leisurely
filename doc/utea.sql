/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : utea

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 01/12/2020 09:05:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `type` tinyint(1) NOT NULL COMMENT '类型1目录2菜单3按钮4外链',
  `identity` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一标志',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '外链url',
  `route` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面路由',
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '时间',
  `parent_id` bigint(20) UNSIGNED ZEROFILL NOT NULL COMMENT '父级id',
  `sort` smallint(5) UNSIGNED ZEROFILL NOT NULL COMMENT '排序',
  `status` tinyint(3) NOT NULL COMMENT '状态1正常2开发3停用',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面存放位置view目录下',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UNIQUE_ROUTE`(`route`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理后台菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES (1, '菜单管理', 2, 'menu', NULL, 'menu', '2020-10-21', 00000000000000000000, 00001, 1, 'basic/menu/menu-mgt', 'ios-apps');
INSERT INTO `admin_menu` VALUES (1325697237384380416, '用户管理', 2, '', '', 'usr', '2020-11-09 15:10:26', 00000000000000000000, 00001, 1, 'basic/usr/usr-mgt', 'md-analytics');
INSERT INTO `admin_menu` VALUES (1325707059915866112, '角色管理', 2, '', '', 'role', '2020-11-09 15:49:28', 00000000000000000000, 00001, 1, 'basic/role/role-mgt', 'md-aperture');
INSERT INTO `admin_menu` VALUES (1325727569051598848, '新增', 3, '', '', 'usr-add', '2020-11-09 17:10:57', 01325697237384380416, 00001, 1, '', '');
INSERT INTO `admin_menu` VALUES (1332252942153236480, '任务管理', 2, '', '', 'task', '2020-11-27 17:20:28', 00000000000000000000, 00001, 1, 'basic/task/task-mgt', 'basic/usr/usr-mgt');

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理平台角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES (1326442011557052416, '测试1', '出手大方2', '2020-11-11 16:29:54');

-- ----------------------------
-- Table structure for admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `admin_menu_id` bigint(20) NOT NULL COMMENT '菜单id',
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理平台角色-菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES (1327182142836523008, 1326442011557052416, 1, '2020-11-13 17:30:55');
INSERT INTO `admin_role_menu` VALUES (1327182142836523009, 1326442011557052416, 1325697237384380416, '2020-11-13 17:30:55');
INSERT INTO `admin_role_menu` VALUES (1327182142836523010, 1326442011557052416, 1325727569051598848, '2020-11-13 17:30:55');
INSERT INTO `admin_role_menu` VALUES (1327182142836523011, 1326442011557052416, 1325707059915866112, '2020-11-13 17:30:55');

-- ----------------------------
-- Table structure for admin_usr
-- ----------------------------
DROP TABLE IF EXISTS `admin_usr`;
CREATE TABLE `admin_usr`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '性别0女1男',
  `age` smallint(3) NULL DEFAULT NULL COMMENT '年龄',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '11位手机号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `salt` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像路径',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态1正常，2禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_usr
-- ----------------------------
INSERT INTO `admin_usr` VALUES (1, 'admin', 1, 1, '1234568578', '1dd50befc130a29bea6307e8959a1a49', '2020', '85hn467t', NULL, 1);
INSERT INTO `admin_usr` VALUES (1326446103243210752, 'abcd', 1, 20, '110', 'e269a88fd08fa54248ca82de04499f96', '2020-11-11 16:46:09', '9zgpyoic', NULL, 1);

-- ----------------------------
-- Table structure for admin_usr_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_usr_role`;
CREATE TABLE `admin_usr_role`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `admin_usr_id` bigint(20) NOT NULL COMMENT 'adminUsrId',
  `admin_role_id` bigint(20) NOT NULL COMMENT 'adminRoleId',
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_usr_role
-- ----------------------------
INSERT INTO `admin_usr_role` VALUES (1326768520243134464, 1326446103243210752, 1326442011557052416, NULL);

-- ----------------------------
-- Table structure for task_info
-- ----------------------------
DROP TABLE IF EXISTS `task_info`;
CREATE TABLE `task_info`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `cron` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '时间corn',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务说明',
  `api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务调度api接口',
  `has_live` tinyint(4) NULL DEFAULT NULL COMMENT '是否使用0否1是',
  `create_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `api_type` tinyint(4) NOT NULL COMMENT 'api接口类型:1get,2post',
  `param` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口参数，json字符串',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of task_info
-- ----------------------------
INSERT INTO `task_info` VALUES (1332136764852293632, '0/10 * * * * ?', '测试1', 'usr/test1', 1, '2020-11-27 09:38:49', 2, '{\"a\":1}');
INSERT INTO `task_info` VALUES (1332249735435800576, '0/5 * * * * ?', '测试', 'usr/test', 1, '2020-11-27 17:07:43', 1, NULL);
INSERT INTO `task_info` VALUES (1332249747708334080, '0/5 * * * * ?', '测试', 'usr/test', 1, '2020-11-27 17:07:46', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
